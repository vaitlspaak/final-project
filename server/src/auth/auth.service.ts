import { ManagersAndAdminsService } from './../common/core/services/managers-and-admins.service';
import { GetUserDTO } from '../models/user/get-user.dto';
import { UserLoginDTO } from '../models/user/user-login.dto';
import { JwtService } from '@nestjs/jwt';
import { Injectable, NotFoundException } from '@nestjs/common';
import { JwtPayload } from './../interfaces/jwt-payload';

@Injectable()
export class AuthService {
  constructor(
    // private readonly clientService: ClientService,
    private readonly managerService: ManagersAndAdminsService,
    private readonly jwtService: JwtService,
  ) {}

  public async signIn(user: UserLoginDTO): Promise<string> {
    const userFound: GetUserDTO = await this.managerService.signIn(user);
    if (userFound) {
      return this.jwtService.sign({
        email: userFound.email,
        role: userFound.role.rolename,
        id: userFound.id,
      });
    } else {
      return null;
    }
  }

  async validateUser(payload: JwtPayload): Promise<GetUserDTO> {
    return await this.managerService.validateUser(payload);
  }
}
