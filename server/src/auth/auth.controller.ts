import { ManagersAndAdminsService } from './../common/core/services/managers-and-admins.service';
import { RolesGuard } from './../common/guards/roles/roles.guard';
import { UserLoginDTO } from '../models/user/user-login.dto';
import { AdminGuard, Roles } from './../common';
import { FileService } from '../common/core/file.service';
import { AuthService } from './auth.service';
// tslint:disable-next-line:max-line-length
import {
  Get,
  Controller,
  UseGuards,
  Post,
  Body,
  FileInterceptor,
  UseInterceptors,
  UploadedFile,
  ValidationPipe,
  UsePipes,
  BadRequestException,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { join } from 'path';
import { unlink } from 'fs';
import { RegisterDTO } from 'src/models/user/register.dto';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly managerService: ManagersAndAdminsService,
  ) {}

  @Get()
  // @Roles('admin', 'manager')
  // @UseGuards(AuthGuard(), RolesGuard)
  async root(): Promise<any> {
    console.log('error');
    return { meesage: 'root' };
  }

  @Post('login')
  async sign(
    @Body(
      new ValidationPipe({
        transform: true,
        whitelist: true,
      }),
    )
    user: UserLoginDTO,
  ): Promise<string> {
    const token = await this.authService.signIn(user);
    if (!token) {
      throw new BadRequestException('Wrong credentials!');
    }

    return JSON.stringify({ token });
  }
}
