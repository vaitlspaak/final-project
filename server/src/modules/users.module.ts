import { OrderController } from './order/order.controller';
import { Manager } from './../data/entities/manager.entity';
import { Client } from './../data/entities/client.entity';
import { AuthModule } from '../auth/auth.module';
import { AuthService } from '../auth/auth.service';
import { CoreModule } from '../common/core/core.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { UsersController } from '../controllers/users.controller';
import { ManagersAndAdminsController } from './managers-and-admins/managers-and-admins.controller';
import { ClientsModule } from './clients/clients.module';
import { OrderModule } from './order/order.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([]),
    CoreModule,
    AuthModule,
    ClientsModule,
    OrderModule,
  ],
  providers: [],
  exports: [],
  controllers: [UsersController, ManagersAndAdminsController, OrderController],
})
export class UsersModule {}
