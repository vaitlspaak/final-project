import { SectorUpdateDTO } from './../../models/sector/sectorUpdate.dto';
import { SectorDTO } from './../../models/sector/sector.dto';
import { Sector } from './../../data/entities/sector.entity';
import { SectorService } from './../../common/core/services/sector.service';
import { Controller, Post, Body, Get, UseGuards } from '@nestjs/common';
import { Roles, RolesGuard } from 'src/common';
import { AuthGuard } from '@nestjs/passport';

@Controller('sector')
export class SectorController {
  constructor(private readonly sectorService: SectorService) {}
  @Get('')
  // @Roles('admin', 'manager')
  // @UseGuards(AuthGuard(), RolesGuard)
  getAllIndustries(): Promise<Sector[]> {
    return this.sectorService.getAllIndustries();
  }
  @Post('add')
  // @Roles('admin', 'manager')
  // @UseGuards(AuthGuard(), RolesGuard)
  createIndustry(@Body() data: SectorDTO): Promise<Sector> {
    return this.sectorService.createIndustry(data);
  }

  @Post('update')
  // @Roles('admin', 'manager')
  // @UseGuards(AuthGuard(), RolesGuard)
  updateIndustry(@Body() industryData: SectorUpdateDTO): Promise<Sector> {
    return this.sectorService.updateIndustry(industryData);
  }
}
