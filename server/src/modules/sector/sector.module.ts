import { SectorController } from './sector.controller';
import { SectorService } from './../../common/core/services/sector.service';
import { Module } from '@nestjs/common';
import { CoreModule } from 'src/common/core/core.module';
import { AuthModule } from 'src/auth/auth.module';

@Module({
  imports: [CoreModule, AuthModule],
  controllers: [SectorController],
})
export class SectorModule {}
