import { GetClientFinancialDataDTO } from './../../models/user/get-client-financial-data.dto';
import { Observable } from 'rxjs';
import { FileService } from './../../common/core/file.service';
import { ClientsService } from './../../common/core/services/clients.service';
import { Client } from './../../data/entities/client.entity';
import { RegisterClientDTO } from './../../models/user/register-client.dto';
import { Controller, Post, Body, Get, Param, Put } from '@nestjs/common';

import {
  UseInterceptors,
  FileInterceptor,
  ValidationPipe,
  UploadedFile,
} from '@nestjs/common';
import { join } from 'path';
import { unlink } from 'fs';
@Controller('')
export class ClientsController {
  constructor(private readonly clientsService: ClientsService) {}

  @Get('admin/clients')
  getAllClients(): Promise<Client[]> {
    return this.clientsService.getAllClients();
  }
  @Post('admin/register/client')
  @UseInterceptors(
    FileInterceptor('photoUrl', {
      limits: FileService.fileLimit(1, 2 * 1024 * 1024),
      storage: FileService.storage(['private', 'uploads', 'images']),
      fileFilter: (req, file, cb) =>
        FileService.fileFilter(req, file, cb, '.png', '.jpg'),
    }),
  )
  async register(
    @Body(
      new ValidationPipe({
        transform: true,
        whitelist: true,
      }),
    )
    client: RegisterClientDTO,
    @UploadedFile()
    file,
  ): Promise<string> {
    const folder = join('.', 'private', 'uploads', 'images');
    if (!file) {
      client.photoUrl = join(folder, 'default.png');
    } else {
      client.photoUrl = join(folder, file.filename);
    }

    try {
      await this.clientsService.registerClient(client);
      return;
    } catch (error) {
      await new Promise((resolve, reject) => {
        // Delete the file if user not found
        if (file) {
          unlink(join('.', file.path), err => {
            if (err) {
              reject(error.message);
            }
            resolve();
          });
        }

        resolve();
      });

      // return error.message;
    }
  }

  @Get('admin/register/client/:id')
  getClientById(@Param() param): Promise<Client> {
    return this.clientsService.getClientById(param.id);
  }

  @Put('admin/register/client/:id')
  updateClientById(
    @Param() param,
    @Body() clientData: Client,
  ): Promise<Client> {
    return this.clientsService.updateClientById(param.id, clientData);
  }

  @Get('admin/register/client/amount/:id')
  getClientAvailableMoney(@Param() param): Promise<number> {
    return this.clientsService.getClientAvailableMoney(param.id);
  }

  @Get('clients/fin/:id')
  getClientsOverallFinancialData(
    @Param() param,
  ): Promise<GetClientFinancialDataDTO> {
    return this.clientsService.getClientsOverallFinancialData(param.id);
  }
}
