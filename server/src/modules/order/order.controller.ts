import { GetClientIdDTO } from './../../models/user/getClientId.dto';
import { GetClosedPositionsDTO } from './../../models/order/getClosedPosition.dto';
import { ClosePositionDTO } from './../../models/order/closePosition.dto';
import { OpenPositionDTO } from './../../models/order/openPosition.dto';
import { Order } from 'src/data/entities/order.entity';
import { OrderService } from './../../common/core/services/order.service';
import { OrderDTO } from './../../models/order/order.dto';
import { Controller, Post, Body, Param, Get, Put } from '@nestjs/common';

@Controller('order')
export class OrderController {
  constructor(private orderService: OrderService) {}
  @Post('')
  openTade(@Body() orderData: OrderDTO): Promise<Order> {
    return this.orderService.createOrder(orderData);
  }

  @Get('open/:id')
  getOpenPositionsByClient(@Param() id): Promise<OpenPositionDTO[]> {
    return this.orderService.getOpenPositionsByClient(id.id);
  }

  @Get('clients')
  getClientsWithOpenedPositions(): Promise<GetClientIdDTO[]> {
    return this.orderService.getClientsWithOpenedPositions();
  }

  @Get('closed/:id')
  getClosedPositionsByClient(
    @Param() id: string,
  ): Promise<GetClosedPositionsDTO[]> {
    return this.orderService.getClosedPositionsByClient(id);
  }

  @Put('close')
  closePosition(@Body() closePositionData: ClosePositionDTO): Promise<Order> {
    return this.orderService.closePosition(
      closePositionData.id,
      closePositionData.closePrice,
    );
  }
}
