import { Client } from './../../data/entities/client.entity';

import { ManagerDataDTO } from './../../models/user/manager-data.dto';
import { RegisterDTO } from './../../models/user/register.dto';
import { Manager } from './../../data/entities/manager.entity';
import { ManagersAndAdminsService } from './../../common/core/services/managers-and-admins.service';
import { Controller, Get, Post, Body, Param } from '@nestjs/common';

@Controller('admin')
export class ManagersAndAdminsController {
  constructor(
    private readonly managerAndAdminServices: ManagersAndAdminsService,
  ) {}

  @Get('managers')
  getAllManagers(): Promise<Manager[]> {
    return this.managerAndAdminServices.getAllManagers();
  }
  @Post('register/manager-admin')
  registerManager(@Body() params: RegisterDTO): Promise<Manager> {
    return this.managerAndAdminServices.registerManagerOrAdmin(params);
  }

  @Get('manager-admin/clients/:id')
  getClietnsByManagerId(@Param() params: ManagerDataDTO): Promise<Client[]> {
    return this.managerAndAdminServices.getClietnsByManagerId(params.id);
  }
}
