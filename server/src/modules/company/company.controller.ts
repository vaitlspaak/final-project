import { Company } from 'src/data/entities/company.entity';
import { CompaniesService } from './../../common/core/services/companies.service';
import { Controller, Get, Post, Body, Param } from '@nestjs/common';

@Controller('company')
export class CompanyController {
  constructor(private readonly companyService: CompaniesService) {}

  @Get('industry/:id')
  getCompaniesByIndustryId(@Param() param): Promise<Company[]> {
    return this.companyService.getCompaniesByIndustry(param.id);
  }

  @Get(':id')
  getCompaniyDataBySymbol(@Param() param): Promise<Company> {
    return this.companyService.getCompanyDataBySymbol(param.id);
  }
}
