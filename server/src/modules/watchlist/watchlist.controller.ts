import { AddWatchListDTO } from './../../models/watchlist/addWatchList.dto';
import { ManagerDataDTO } from './../../models/user/manager-data.dto';
import { Watchlist } from 'src/data/entities/watchlist.entity';
import { Client } from './../../data/entities/client.entity';
import { Company } from '../../data/entities/company.entity';
import { WatchlistService } from '../../common/core/services/watchlist.service';
import { Controller, Get, UseGuards, Body, Param, Post } from '@nestjs/common';
import { Roles, RolesGuard } from 'src/common';
import { AuthGuard } from '@nestjs/passport';

@Controller('watchlist')
export class WatchlistController {
  constructor(private readonly watchlistService: WatchlistService) {}

  @Get('manager/:id')
  // @Roles('manager')
  // @UseGuards(AuthGuard(), RolesGuard)
  async getWatchlistsByManagerId(
    @Param() manager: ManagerDataDTO,
  ): Promise<Watchlist[]> {
    return await this.watchlistService.getWatchlistsByManagerId(manager.id);
  }

  @Post('add')
  // // @Roles('manager')
  // // @UseGuards(AuthGuard(), RolesGuard)
  async addWatchList(
    @Body() watchListData: AddWatchListDTO,
  ): Promise<Watchlist> {
    return await this.watchlistService.addWatchList(watchListData);
  }

  // @Get('watchlist/removecompany')
  // @Roles('manager')
  // @UseGuards(AuthGuard(), RolesGuard)
  // async removeCompany(): Promise<object> {
  //   // for testing purposes - this will be taken from the body
  //   const company1 = new Company();
  //   company1.id = 'newcompanyid';
  //   company1.name = 'newcompanyname';
  //   // company1.closedate = new Date();
  //   //
  //   return await this.watchlistService.removeCompany(company1.id);
  // }
}
