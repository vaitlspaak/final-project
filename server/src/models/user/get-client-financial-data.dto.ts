export class GetClientFinancialDataDTO {
  currentOverallBalance: number;
  currentlyInvestedMoney: number;
  moneyAvailableForInvestment: number;
}
