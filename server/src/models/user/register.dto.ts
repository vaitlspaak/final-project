import { Role } from './../../data/entities/role.entity';
import { IsString, Matches, IsEmail } from 'class-validator';

export class RegisterDTO {
  @IsString()
  firstname: string;
  @IsString()
  lastname: string;
  @IsEmail()
  email: string;
  dateregistered: Date;
  role: Role;
  @IsString()
  @Matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/)
  password: string;
}
