import { Role } from './../../data/entities/role.entity';
import {
  IsString,
  Length,
  Matches,
  IsOptional,
  IsEmail,
} from 'class-validator';
export class GetUserDTO {
  email: string;

  password: string;

  role: Role;

  id: string;
}
