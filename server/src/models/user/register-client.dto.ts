import { Manager } from './../../data/entities/manager.entity';
import { Funds } from './../../data/entities/funds.entity';
import { Role } from './../../data/entities/role.entity';
import { IsString, Matches, IsEmail, IsNumber } from 'class-validator';

export class RegisterClientDTO {
  @IsString()
  firstname: string;
  @IsString()
  lastname: string;
  @IsEmail()
  email: string;
  @IsString()
  address: string;
  @IsString()
  age: number;
  @IsString()
  managerId: string;

  @IsString()
  photoUrl: string;
  @IsString()
  funds: number;
}
