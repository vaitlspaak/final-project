import { IsString } from 'class-validator';
export class AddWatchListDTO {
  @IsString()
  name: string;
  @IsString()
  ownerId: string;
  @IsString()
  managerId: string;
}
