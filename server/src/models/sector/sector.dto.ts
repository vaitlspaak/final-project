import { IsString } from 'class-validator';

export class SectorDTO {
  @IsString()
  name: string;
}
