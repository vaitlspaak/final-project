import { IsString } from 'class-validator';

export class SectorUpdateDTO {
  @IsString()
  id: string;

  @IsString()
  name: string;
}
