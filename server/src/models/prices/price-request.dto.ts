import { IsString, IsNumber, IsDate, IsOptional } from 'class-validator';

export class PriceRequestDTO {
  @IsString()
  symbol: string;
}
