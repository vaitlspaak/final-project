export class OrderDTO {
  openDate: Date;
  closeDate: Date;
  price: number;
  type: string;
  units: number;
  companyId: string;
  clientId: string;
  status: string;
}
