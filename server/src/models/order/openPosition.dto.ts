export class OpenPositionDTO {
  id: string;
  companyName: string;
  units: number;
  price: number;
  type: string;
  invested: number;
}
