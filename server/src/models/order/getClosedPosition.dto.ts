export class GetClosedPositionsDTO {
  companyName: string;
  units: number;
  openPrice: number;
  closePrice: number;
  invested: number;
  openTime: string;
  closeTime: string;

  profitlossmoney: number;
  profitlosspercentage: number;
}
