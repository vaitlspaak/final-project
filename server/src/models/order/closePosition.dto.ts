export class ClosePositionDTO {
  id: string;
  closePrice: number;
}
