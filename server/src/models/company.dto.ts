import {
  IsString,
  Length,
  Matches,
  IsOptional,
  IsEmail,
} from 'class-validator';
import { Sector } from '../data/entities/sector.entity';

export class CompanyDTO {
  @IsString()
  name: string;

  @IsString()
  symbol: string;

  @IsString()
  sector: Sector;

  @IsString()
  ipoyear: string;

  @IsString()
  industry: string;
  @IsString()
  summary: string;
}
