import { OrderController } from './modules/order/order.controller';
import { UsersModule } from './modules/users.module';
import { SectorService } from './common/core/services/sector.service';
import { SectorController } from './modules/sector/sector.controller';
import { CompanyController } from './modules/company/company.controller';
import { ClientsService } from './common/core/services/clients.service';
import { ClientsController } from './modules/clients/clients.controller';
import { ManagersAndAdminsController } from './modules/managers-and-admins/managers-and-admins.controller';
import { SectorModule } from './modules/sector/sector.module';
import { ManagersAndAdminsService } from './common/core/services/managers-and-admins.service';
import { ConfigService } from './config/config.service';
import { Module, HttpModule } from '@nestjs/common';
import { ConfigModule } from './config/config.module';
import { AuthModule } from './auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CoreModule } from './common/core/core.module';
import { DatabaseModule } from './database/database.module';
import { WatchlistModule } from './modules/watchlist/watchlist.module';
import { PricesModule } from './modules/prices/prices.module';
@Module({
  imports: [
    ConfigModule,
    HttpModule,
    AuthModule,
    DatabaseModule,
    CoreModule,
    DatabaseModule,
    WatchlistModule,
    PricesModule,
  ],
  controllers: [
    ManagersAndAdminsController,
    ClientsController,
    CompanyController,
    SectorController,
    OrderController,
  ],
  providers: [ManagersAndAdminsService, ClientsService],
})
export class AppModule {}
