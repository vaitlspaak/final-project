import { ClientsService } from './services/clients.service';
import { SectorService } from './services/sector.service';
import { ManagersAndAdminsService } from './services/managers-and-admins.service';
import { Sector } from './../../data/entities/sector.entity';
import { Company } from './../../data/entities/company.entity';
import { Manager } from './../../data/entities/manager.entity';
import { Client } from './../../data/entities/client.entity';
import { Funds } from './../../data/entities/funds.entity';
import { Status } from './../../data/entities/status.entity';
import { Watchlist } from './../../data/entities/watchlist.entity';
import { Role } from './../../data/entities/role.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { FileService } from './file.service';
import { Order } from 'src/data/entities/order.entity';
import { OrderService } from './services/order.service';
import { CompaniesService } from './services/companies.service';
import { FundsService } from './services/funds.service';
import { Settings } from './../../data/entities/settings.entity';
import { Price } from './../../data/entities/prices.entity';
import { WatchlistService } from './services/watchlist.service';
import { PricesService } from './services/prices.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Manager,
      Client,
      Company,
      Sector,
      Watchlist,
      Order,
      Status,
      Role,
      Funds,
      Settings,
      Price,
    ]),
  ],
  providers: [
    ManagersAndAdminsService,
    FileService,
    CompaniesService,
    OrderService,
    WatchlistService,
    FundsService,
    SectorService,
    PricesService,
    ClientsService,
  ],
  exports: [
    ManagersAndAdminsService,
    FileService,
    CompaniesService,
    OrderService,
    WatchlistService,
    FundsService,
    SectorService,
    PricesService,
    ClientsService,
  ],
})
export class CoreModule {}
