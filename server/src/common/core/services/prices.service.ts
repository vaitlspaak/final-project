import {
  Injectable,
  HttpException,
  HttpStatus,
  BadRequestException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Between, MoreThan } from 'typeorm';
import { Price } from 'src/data/entities/prices.entity';
import { Company } from 'src/data/entities/company.entity';

@Injectable()
export class PricesService {
  constructor(
    @InjectRepository(Price)
    private readonly priceRepository: Repository<Price>,
    @InjectRepository(Company)
    private readonly companyRepository: Repository<Company>,
  ) {}

  async getCompanyPrices(symbol: string): Promise<Price[]> {
    const companyFound = await this.companyRepository.findOne({
      where: { symbol: symbol },
    });
    if (!companyFound) {
      throw new HttpException("Company doesn't exist!", HttpStatus.NOT_FOUND);
    }

    const result = await this.priceRepository.find({
      where: { company: companyFound },
    });
    return result;
  }

  async getLastPricePerCompany(): Promise<Price[]> {
    const companies = await this.companyRepository.find({});
    const result = [];

    for (const company of companies) {
      try {
        const price = await this.priceRepository.findOne({
          where: { company },
          order: { opendate: 'DESC' },
        });
        result.push(price);
      } catch (e) {
        // Log error if necessary
      }
    }

    return result;
  }
}
