import { GetClientFinancialDataDTO } from './../../../models/user/get-client-financial-data.dto';
import { Order } from 'src/data/entities/order.entity';
import { RegisterClientDTO } from './../../../models/user/register-client.dto';
import { Client } from '../../../data/entities/client.entity';
import { Manager } from '../../../data/entities/manager.entity';
import { ClientRegisterDTO } from './../../../models/user/client-register.dto';
import { GetUserDTO } from '../../../models/user/get-user.dto';
import { UserLoginDTO } from '../../../models/user/user-login.dto';
import {
  Injectable,
  BadRequestException,
  HttpStatus,
  HttpException,
} from '@nestjs/common';
import { Repository, AdvancedConsoleLogger, getConnection } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { JwtPayload } from '../../../interfaces/jwt-payload';
import { Role } from '../../../data/entities/role.entity';
import { Funds } from '../../../data/entities/funds.entity';
import { getRepository } from 'typeorm';

@Injectable()
export class ClientsService {
  constructor(
    @InjectRepository(Client)
    private readonly clientRepository: Repository<Client>,
    @InjectRepository(Role)
    private readonly roleRepository: Repository<Role>,
    @InjectRepository(Manager)
    private readonly managersRepository: Repository<Manager>,
    @InjectRepository(Funds)
    private readonly fundsRepository: Repository<Funds>,
    @InjectRepository(Order)
    private readonly ordersRepository: Repository<Order>,
  ) {}

  // ==> Only admin can register new client and managers profiles

  async registerClient(client: RegisterClientDTO) {
    try {
      const clientFound = await this.clientRepository.findOne({
        where: { email: client.email },
      });

      if (clientFound) {
        throw new BadRequestException('Client already exists.');
      }

      const newClient = await this.clientRepository.create();
      newClient.dateregistered = new Date();
      newClient.email = client.email;
      newClient.firstname = client.firstname;
      newClient.lastname = client.lastname;
      newClient.age = client.age;
      newClient.address = client.address;
      newClient.photoUrl = client.photoUrl;
      newClient.manager = await this.managersRepository.findOne({
        where: { id: client.managerId },
      });
      newClient.role = await this.roleRepository.findOne({
        where: { rolename: 'client' },
      });

      const fundsToAdd = await this.fundsRepository.create();
      fundsToAdd.currentamount = client.funds;
      await this.fundsRepository.save(fundsToAdd);

      // const fundsToAdd = new Funds();
      // fundsToAdd.currentamount = client.funds;
      newClient.funds = fundsToAdd;
      const result = await this.clientRepository.save(newClient);

      return result;
    } catch (error) {
      throw new BadRequestException('Cannot add client to the database.');
    }
  }

  async getAllClients(): Promise<Client[]> {
    return this.clientRepository.find({});
  }

  async getClientById(id: string): Promise<any> {
    try {
      const user = await this.clientRepository.findOneOrFail({ id });
      return user;
    } catch (error) {
      throw new BadRequestException('No such client in the DB');
    }
  }

  async getClientAvailableMoney(id: string): Promise<number> {
    try {
      const user = await this.clientRepository.findOne({ id });

      if (!user) {
        throw new HttpException(
          'No such client in the DB',
          HttpStatus.NOT_FOUND,
        );
      }

      const ordersAmount = await getRepository(Order)
        .createQueryBuilder('orders')
        .select('SUM(orders.price * orders.units)', 'ordersAmount')
        .where('orders.clientId= :orderId', { orderId: id })
        .andWhere('orders.status = :status', { status: 'open' })
        .getRawOne();

      return user.funds.currentamount - +ordersAmount.ordersAmount;
    } catch (error) {
      throw new BadRequestException(`Error getting client's available money`);
    }
  }

  async updateClientById(clientId: string, clientData: any): Promise<Client> {
    const userFound = await this.clientRepository.findOne({
      where: { id: clientId },
    });

    if (!userFound) throw new BadRequestException('No such client in the DB');

    userFound.firstname = clientData.firstname;
    userFound.lastname = clientData.lastname;
    userFound.age = clientData.age;
    userFound.email = clientData.email;
    userFound.address = clientData.address;
    if (clientData.managerId !== 'Keep current manager') {
      userFound.manager = await this.managersRepository.findOne({
        where: { id: clientData.managerId },
      });
    }
    userFound.funds.currentamount = clientData.funds;
    return await this.clientRepository.save(userFound);
  }

  async getClientsOverallFinancialData(
    clientId: string,
  ): Promise<GetClientFinancialDataDTO> {
    const openedOrdersAmount = await getRepository(Order)
      .createQueryBuilder('orders')
      .select('SUM(orders.price * orders.units)', 'openedOrdersAmount')
      .where('orders.clientId= :id', { id: clientId })
      .andWhere('orders.status = :status', { status: 'open' })
      .getRawOne();

    const closedOrdersAmount = await getRepository(Order)
      .createQueryBuilder('orders')
      .select('SUM(orders.closePrice * orders.units)', 'closedOrdersAmount')
      .where('orders.clientId= :id', { id: clientId })
      .andWhere('orders.status = :status', { status: 'closed' })
      .getRawOne();

    const clientData = await this.clientRepository.findOne({
      where: { clientId },
    });

    const clientFinancialData = new GetClientFinancialDataDTO();
    clientFinancialData.currentOverallBalance =
      +openedOrdersAmount.openedOrdersAmount +
      closedOrdersAmount.closedOrdersAmount;

    clientFinancialData.currentlyInvestedMoney = +openedOrdersAmount.openedOrdersAmount;

    clientFinancialData.moneyAvailableForInvestment =
      closedOrdersAmount.closedOrdersAmount +
      clientData.funds.currentamount -
      +openedOrdersAmount.openedOrdersAmount;

    return clientFinancialData;
  }
}
