import { Sector } from '../../../data/entities/sector.entity';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Company } from 'src/data/entities/company.entity';
import { Repository } from 'typeorm';
import { CompanyDTO } from 'src/models/company.dto';
import { Watchlist } from 'src/data/entities/watchlist.entity';

@Injectable()
export class CompaniesService {
  constructor(
    @InjectRepository(Company)
    private readonly companyRepository: Repository<Company>,
    @InjectRepository(Sector)
    private readonly sectorRepository: Repository<Sector>,
    @InjectRepository(Watchlist)
    private readonly watchlistRepository: Repository<Watchlist>,
  ) {}

  async createCompany(companyDTO: CompanyDTO) {
    const companyFound = await this.companyRepository.findOne({
      where: { name: companyDTO.name },
    });

    if (companyFound) {
      throw new HttpException(
        'Company already exists!',
        HttpStatus.BAD_REQUEST,
      );
    }

    // const industry = await this.industryRepository.findOne({
    //   where: { id: companyDTO.industryId },
    // });
    // if (!industry) {
    //   throw new HttpException('Industry not found!', HttpStatus.NOT_FOUND);
    // }

    const company = new Company();
    company.name = companyDTO.name;
    company.symbol = companyDTO.symbol;
    company.ipoyear = companyDTO.ipoyear;
    company.summary = companyDTO.summary;
    company.industry = companyDTO.industry;
    company.sector = companyDTO.sector;
    // company.closedate = new Date();

    await this.companyRepository.create(company);

    return await this.companyRepository.save(company);
  }

  async updateCompany(id: string, companyDTO: Partial<CompanyDTO>) {
    const companyFound = await this.companyRepository.findOne({
      where: { id },
    });

    if (!companyFound) {
      throw new HttpException('Company not found!', HttpStatus.NOT_FOUND);
    }

    companyFound.name = companyDTO.name;
    companyFound.symbol = companyDTO.symbol;
    companyFound.ipoyear = companyDTO.ipoyear;
    companyFound.summary = companyDTO.summary;
    companyFound.industry = companyDTO.industry;
    companyFound.sector = companyDTO.sector;

    if (companyDTO.sector) {
      const sector = await this.sectorRepository.findOne({
        where: { id: companyDTO.sector },
      });
      if (!sector) {
        throw new HttpException('Industry not found!', HttpStatus.NOT_FOUND);
      }
      companyFound.sector = sector;
    }

    await this.companyRepository.save(companyFound);

    return await this.companyRepository.findOne({
      where: { id },
    });
  }

  async getCompaniesByIndustry(id: string) {
    const sector = await this.sectorRepository.findOne({ where: { id } });

    if (!sector) {
      throw new HttpException('Industry not found!', HttpStatus.NOT_FOUND);
    }

    return await this.companyRepository.find({ where: { sector } });
  }

  async getCompanyDataBySymbol(symbol: string) {
    const company = await this.companyRepository.findOne({ where: { symbol } });

    if (!company) {
      throw new HttpException('Company not found!', HttpStatus.NOT_FOUND);
    }

    return company;
  }
  async getCompanyTimesListed(id: string) {
    const companyFound = await this.companyRepository.findOne({
      where: { id },
    });

    if (!companyFound) {
      throw new HttpException('Company not found!', HttpStatus.NOT_FOUND);
    }

    const companies = await this.watchlistRepository.find({
      where: { companyFound },
    });

    return companies.length;
  }
}
