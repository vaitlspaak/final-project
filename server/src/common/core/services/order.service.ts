import { GetClientIdDTO } from './../../../models/user/getClientId.dto';
import { GetClosedPositionsDTO } from './../../../models/order/getClosedPosition.dto';
import { OpenPositionDTO } from './../../../models/order/openPosition.dto';
import { Client } from './../../../data/entities/client.entity';
import { Status } from './../../../data/entities/status.entity';
import { Company } from './../../../data/entities/company.entity';
import {
  Injectable,
  HttpException,
  HttpStatus,
  BadRequestException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Order } from '../../../data/entities/order.entity';
import { OrderDTO } from '../../../models/order/order.dto';
import { getRepository } from 'typeorm';

@Injectable()
export class OrderService {
  constructor(
    @InjectRepository(Order)
    private readonly orderRepository: Repository<Order>,
    @InjectRepository(Client)
    private readonly clientRepository: Repository<Client>,
    @InjectRepository(Company)
    private readonly companyRepository: Repository<Company>,
    @InjectRepository(Status)
    private readonly statusRepository: Repository<Status>,
  ) {}

  async createOrder(order: OrderDTO): Promise<Order> {
    const foundClient: Client = await this.clientRepository.findOne({
      where: { id: order.clientId },
    });
    if (!foundClient) {
      throw new HttpException('Client not found!', HttpStatus.NOT_FOUND);
    }

    const foundCompany: Company = await this.companyRepository.findOneOrFail({
      where: { id: order.companyId },
    });
    if (!foundCompany) {
      throw new HttpException('Company not found!', HttpStatus.NOT_FOUND);
    }

    const amountNeeded = order.price * order.units;
    console.log(`amountNeeded=${amountNeeded}`);

    if (amountNeeded < foundClient.funds.currentamount) {
      try {
        const createOrder: Order = await this.orderRepository.create();
        createOrder.opendate = new Date();
        createOrder.closedate = null;
        createOrder.price = order.price;
        createOrder.type = order.type;
        createOrder.units = order.units;
        createOrder.client = foundClient;
        createOrder.status = order.status;
        createOrder.company = foundCompany;
        createOrder.closeprice = 0;
        console.log(createOrder);

        return await this.orderRepository.save(createOrder);
      } catch (error) {
        throw new HttpException('Cannot create order', HttpStatus.BAD_REQUEST);
      }
    } else {
      throw new HttpException('You are out of money', HttpStatus.BAD_REQUEST);
    }
  }

  async getOrdersAll() {
    try {
      const foundOrders = await this.orderRepository.find();
      return foundOrders;
    } catch (error) {
      throw new HttpException('Open orders not found!', HttpStatus.NOT_FOUND);
    }
  }

  async getOpenPositionsByClient(clientId: string): Promise<OpenPositionDTO[]> {
    console.log(clientId);
    const clientFound = await this.clientRepository.findOne({
      where: { id: clientId },
    });

    console.log(clientFound);
    const foundPositions = await this.orderRepository.find({
      where: { client: clientFound },
    });

    console.log(foundPositions);

    if (!foundPositions) {
      throw new HttpException(
        'There are no active positions!',
        HttpStatus.NOT_FOUND,
      );
    }
    const openPosition: OpenPositionDTO[] = foundPositions.map(position => {
      const positionToMap = new OpenPositionDTO();
      positionToMap.id = position.id;
      positionToMap.companyName = `${position.company.name}(${
        position.company.symbol
      })`;

      positionToMap.price = position.price;
      positionToMap.type = position.type;
      positionToMap.units = position.units;
      positionToMap.invested = position.price * position.units;
      return positionToMap;
    });

    return openPosition;
  }

  async getClosedPositionsByClient(
    id: string,
  ): Promise<GetClosedPositionsDTO[]> {
    const foundPositions = await this.orderRepository.find({
      where: { clientId: id, status: 'closed' },
    });

    if (!foundPositions) {
      throw new HttpException(
        'There are no closed positions!',
        HttpStatus.NOT_FOUND,
      );
    }
    const closedPositions: GetClosedPositionsDTO[] = foundPositions.map(
      position => {
        const positionToMap = new GetClosedPositionsDTO();
        positionToMap.companyName = `${position.company.name}(${
          position.company.symbol
        })`;

        positionToMap.openPrice = position.price;
        positionToMap.closePrice = position.closeprice;
        positionToMap.units = position.units;
        positionToMap.invested = position.price * position.units;
        positionToMap.closeTime = position.closedate.toDateString();
        positionToMap.openTime = position.opendate.toDateString();
        positionToMap.profitlossmoney = +(
          position.closeprice * position.units -
          position.price * position.units
        ).toFixed(2);

        positionToMap.profitlosspercentage = +(
          (positionToMap.profitlossmoney / (position.price * position.units)) *
          100
        ).toFixed(2);

        return positionToMap;
      },
    );

    return closedPositions;
  }

  async closePosition(orderId: string, closePrice: number): Promise<Order> {
    console.log(`ClosePosition - OrderId:${orderId}`);
    console.log(`ClosePosition - ClosePrice:${closePrice}`);

    const foundPositions = await this.orderRepository.findOne({
      where: { id: orderId },
    });

    if (!foundPositions) {
      throw new HttpException('Position not found!', HttpStatus.NOT_FOUND);
    }

    foundPositions.status = 'closed';
    foundPositions.closedate = new Date();
    foundPositions.closeprice = closePrice;
    this.orderRepository.create(foundPositions);
    try {
      return await this.orderRepository.save(foundPositions);
    } catch {
      throw new HttpException('Cannot update!', HttpStatus.BAD_REQUEST);
    }
  }

  async getClientsWithOpenedPositions(): Promise<GetClientIdDTO[]> {
    const clientsId = await getRepository(Order)
      .createQueryBuilder('orders')
      .select('DISTINCT(orders.clientId)', 'clientsId')
      .where('orders.status= :status', { status: 'open' })
      .getRawMany();

    const clientsData: GetClientIdDTO[] = [];

    clientsId.map(client => {
      const currentClientData = new GetClientIdDTO();
      console.log(client.clientsId);
      currentClientData.id = client.clientsId;
      console.log(`currentClientData.id:${currentClientData.id}`);

      clientsData.push(currentClientData);
    });
    console.log(`Client's data: ${clientsData}`);
    return clientsData;
  }
}
