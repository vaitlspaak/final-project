import { Client } from './../../../data/entities/client.entity';
import { Funds } from './../../../data/entities/funds.entity';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { FundDTO } from '../../../models/funds/fund.dto';
import { AddSubstractFundDTO } from '../../../models/funds/add-substract-fund.dto';

@Injectable()
export class FundsService {
  constructor(
    @InjectRepository(Funds)
    private readonly fundRepository: Repository<Funds>,
    @InjectRepository(Client)
    private readonly clientRepository: Repository<Client>,
  ) {}
  async createFund(fundDTO: FundDTO) {
    const foundClient: Client = await this.clientRepository.findOne({
      id: fundDTO.client_id,
    });
    if (!foundClient) {
      throw new HttpException('Client not found!', HttpStatus.NOT_FOUND);
    }

    const createFund: Funds = await this.fundRepository.create();
    createFund.currentamount = fundDTO.amount;

    await this.fundRepository.save(createFund);
    return await this.clientRepository.update(
      { id: fundDTO.client_id },
      { funds: createFund },
    );
  }

  async addToFund(fundDTO: AddSubstractFundDTO) {
    const foundFund: Funds = await this.fundRepository.findOne({
      id: fundDTO.id,
    });
    if (!foundFund) {
      throw new HttpException('Fund not found!', HttpStatus.NOT_FOUND);
    }

    return await this.fundRepository.update(
      { id: fundDTO.id },
      { currentamount: foundFund.currentamount + fundDTO.amount },
    );
  }
  async substractFund(fundDTO: AddSubstractFundDTO) {
    const foundFund: Funds = await this.fundRepository.findOne({
      id: fundDTO.id,
    });
    if (!foundFund) {
      throw new HttpException('Fund not found!', HttpStatus.NOT_FOUND);
    }
    if (foundFund.currentamount < fundDTO.amount) {
      throw new Error(
        'Current amount is less than the amount you want to extract',
      );
    }

    return await this.fundRepository.update(
      { id: fundDTO.id },
      { currentamount: foundFund.currentamount - fundDTO.amount },
    );
  }

  async currentFund(client_id: string) {
    const clientFound = await this.clientRepository.findOne({
      where: { id: client_id },
    });

    if (!clientFound) {
      throw new HttpException('Client not found!', HttpStatus.NOT_FOUND);
    }

    return await clientFound.funds.currentamount;
  }
}
