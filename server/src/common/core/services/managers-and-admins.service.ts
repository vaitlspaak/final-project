import { Client } from '../../../data/entities/client.entity';
import { Manager } from '../../../data/entities/manager.entity';
import { ClientRegisterDTO } from './../../../models/user/client-register.dto';
import { GetUserDTO } from '../../../models/user/get-user.dto';
import { UserLoginDTO } from '../../../models/user/user-login.dto';
import { Injectable, BadRequestException } from '@nestjs/common';
import { Repository, AdvancedConsoleLogger } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { JwtPayload } from '../../../interfaces/jwt-payload';
import { Role } from '../../../data/entities/role.entity';
import { Funds } from '../../../data/entities/funds.entity';
import { RegisterDTO } from '../../../models/user/register.dto';

@Injectable()
export class ManagersAndAdminsService {
  constructor(
    @InjectRepository(Manager)
    private readonly managerRepository: Repository<Manager>,
    @InjectRepository(Client)
    private readonly clientRepository: Repository<Client>,
    @InjectRepository(Role)
    private readonly roleRepository: Repository<Role>,
    @InjectRepository(Funds)
    private readonly fundsRepository: Repository<Funds>,
  ) {}

  // ==> Only admin can register new client and managers profiles

  async registerManagerOrAdmin(user: RegisterDTO) {
    try {
      const userFound = await this.managerRepository.findOne({
        where: { email: user.email },
      });

      if (userFound) {
        throw new BadRequestException('User already exist.');
      }

      const newManager = await this.managerRepository.create();
      newManager.dateregistered = new Date();
      newManager.email = user.email;
      newManager.firstname = user.firstname;
      newManager.lastname = user.lastname;
      const managerRole = await this.roleRepository.findOne({
        where: { rolename: user.role },
      });
      newManager.role = managerRole;
      newManager.password = user.password = await bcrypt.hash(
        user.password,
        10,
      );

      const result = await this.managerRepository.save(newManager);

      return result;
    } catch (error) {
      throw new BadRequestException('Cannot add manager/admin to database.');
    }
  }

  async getClietnsByManagerId(id: string): Promise<Client[]> {
    const managerFound = await this.managerRepository.findOne({
      where: { managerId: id },
    });

    if (!managerFound) {
      throw new BadRequestException('No such manager in the DB');
    }

    // console.log(managerFound);
    return this.clientRepository.find({ where: { manager: managerFound } });
  }

  async validateUser(payload: JwtPayload): Promise<GetUserDTO> {
    const userFound: any = await this.managerRepository.findOne({
      where: { email: payload.email },
    });
    return userFound;
  }

  async signIn(user: UserLoginDTO): Promise<GetUserDTO> {
    const userFound: GetUserDTO = await this.managerRepository.findOne({
      where: { email: user.email },
    });

    if (userFound) {
      const result = await bcrypt.compare(user.password, userFound.password);
      if (result) {
        return userFound;
      }
    }

    return null;
  }

  async getAllManagers(): Promise<Manager[]> {
    return this.managerRepository.find({ where: { rolename: 'manager' } });
  }

  async getAllAdmins() {
    return this.managerRepository.find({ where: { rolename: 'admin' } });
  }

  async getManagerOrAdminById(id: string): Promise<Manager> {
    try {
      const user = await this.managerRepository.findOneOrFail({ id });
      return user;
    } catch (error) {
      throw new BadRequestException('No such manager or admin');
    }
  }
}
