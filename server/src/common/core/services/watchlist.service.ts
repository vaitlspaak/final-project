import { ClientsService } from './clients.service';
import { ManagersAndAdminsService } from './managers-and-admins.service';
import { AddWatchListDTO } from './../../../models/watchlist/addWatchList.dto';
import { Watchlist } from './../../../data/entities/watchlist.entity';
import { Manager } from './../../../data/entities/manager.entity';
import { Company } from '../../../data/entities/company.entity';
import {
  Injectable,
  HttpStatus,
  HttpException,
  BadRequestException,
} from '@nestjs/common';
import { Repository, Not, IsNull } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { watch } from 'fs';
@Injectable()
export class WatchlistService {
  private currentWatchlist: Watchlist[];

  constructor(
    @InjectRepository(Manager)
    private readonly managerRepository: Repository<Manager>,
    @InjectRepository(Watchlist)
    private readonly watchlistRepository: Repository<Watchlist>,
    private readonly managerService: ManagersAndAdminsService,
    private readonly clientService: ClientsService,
  ) {}

  async getWatchlistsByManagerId(managerID: string): Promise<Watchlist[]> {
    const foundManager = await this.managerRepository.findOne({
      id: managerID,
    });
    if (!foundManager) {
      throw new HttpException('There is no such user', HttpStatus.NOT_FOUND);
    }

    const foundWatchlists = await this.watchlistRepository.find({
      where: { foundManager },
    });
    if (!foundWatchlists) {
      throw new HttpException(
        'This manager has no watchlist',
        HttpStatus.NOT_FOUND,
      );
    }

    // saving watchlist companies until we update the watchlit route
    this.currentWatchlist = foundWatchlists;

    return foundWatchlists;
  }

  async addWatchList(watchListData: AddWatchListDTO): Promise<Watchlist> {
    // console.log(watchListData);

    try {
      const manager = await this.managerService.getManagerOrAdminById(
        watchListData.managerId,
      );

      let client;
      if (watchListData.ownerId !== null) {
        console.log(`==============NOT NULL==============`);
        client = await this.clientService.getClientById(watchListData.ownerId);
        // console.log(client);
        const watchlistFound = await this.watchlistRepository.find({
          where: {
            manager,
            client,
            name: watchListData.name,
          },
        });

        // console.log(watchlistFound.length);

        if (watchlistFound.length !== 0) {
          throw new HttpException('Watchlist Already exists', HttpStatus.FOUND);
        }
      } else {
        console.log(`==============NULL==============`);

        const watchlistFound = await this.watchlistRepository.find({
          where: {
            manager,
            client: null,
            name: watchListData.name,
          },
        });
        // console.log(watchlistFound.length);

        if (watchlistFound.length !== 0) {
          throw new HttpException('Watchlist Already exists', HttpStatus.FOUND);
        }
      }

      const newWatchlist = await this.watchlistRepository.create();

      if (client) {
        newWatchlist.manager = manager;
        newWatchlist.client = client;
        newWatchlist.name = watchListData.name;
      } else {
        newWatchlist.manager = manager;
        newWatchlist.client = null;
        newWatchlist.name = watchListData.name;
      }
      console.log(newWatchlist);

      return await this.watchlistRepository.save(newWatchlist);
    } catch (error) {
      console.log(`error details: Error on method addWatchlist\n`),
        console.log(`error message: ${error}`);
      throw new HttpException(
        `Cannot add watchlist. ${error}`,
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  // async removeCompany(companyId: string): Promise<object> {
  //   try {
  //     const watchlistCompanies = await this.currentWatchlist.companies;
  //     const initialNumberOfCompanies = watchlistCompanies.length;

  //     for (const [index, company] of watchlistCompanies.entries()) {
  //       if (company.id === companyId) {
  //         watchlistCompanies.splice(index, 1);
  //         break;
  //       }
  //     }

  //     if (initialNumberOfCompanies === watchlistCompanies.length) {
  //       throw new HttpException(
  //         'Company not found in watchlist',
  //         HttpStatus.NOT_FOUND,
  //       );
  //     }
  //     // updating watchlist in service also in db
  //     await this.watchlistRepository.save(this.currentWatchlist);

  //     return {
  //       result: `Company with id:${companyId} has been removed from watchlist!`,
  //     };
  //   } catch (error) {
  //     console.log(`error details: Error on method removeCompany\n`),
  //       console.log(`error message: ${error}`);
  //     throw new HttpException('Cannot remove company', HttpStatus.BAD_REQUEST);
  //   }
  // }
}
