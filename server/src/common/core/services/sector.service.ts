import { SectorUpdateDTO } from './../../../models/sector/sectorUpdate.dto';
import { SectorDTO } from './../../../models/sector/sector.dto';
import { Sector } from './../../../data/entities/sector.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { Result } from 'range-parser';
@Injectable()
export class SectorService {
  constructor(
    @InjectRepository(Sector)
    private readonly sectorRepository: Repository<Sector>,
  ) {}

  async createIndustry(industryToAdd: SectorDTO): Promise<Sector> {
    const industryFound = await this.sectorRepository.findOne({
      where: { name: industryToAdd.name },
    });

    if (industryFound) {
      throw new HttpException(
        'Industry already exists!',
        HttpStatus.BAD_REQUEST,
      );
    }

    const industry: Sector = new Sector();
    industry.name = industryToAdd.name;

    await this.sectorRepository.create(industry);
    const result = await this.sectorRepository.save(industry);
    return result;
  }
  async updateIndustry(industryData: SectorUpdateDTO): Promise<Sector> {
    const industryFound = await this.sectorRepository.findOne({
      where: { id: industryData.id },
    });

    if (!industryFound) {
      throw new HttpException(
        'There is no industry with this ID in the database',
        HttpStatus.BAD_REQUEST,
      );
    }

    industryFound.name = industryData.name;

    const result = await this.sectorRepository.save(industryFound);

    return result;
  }

  async getAllIndustries(): Promise<Sector[]> {
    const industries: Sector[] = await this.sectorRepository.find({});

    if (!industries) {
      return [];
    }

    return industries;
  }
}
