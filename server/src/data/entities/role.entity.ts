import { Manager } from './manager.entity';
import { Column, PrimaryGeneratedColumn, Entity, OneToMany } from 'typeorm';

@Entity({
  name: 'roles',
})
export class Role {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ default: '', unique: true })
  rolename: string;

  @OneToMany(type => Manager, manager => manager.role)
  users: Promise<Manager[]>;
}
