import { Company } from './company.entity';
import { Column, PrimaryGeneratedColumn, Entity, OneToMany } from 'typeorm';

@Entity({
  name: 'sectors',
})
export class Sector {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ default: '', unique: true })
  name: string;

  @OneToMany(type => Company, company => company.sector)
  company: Promise<Company[]>;
}
