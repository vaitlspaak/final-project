import { Manager } from './manager.entity';
import { PrimaryGeneratedColumn, Entity, OneToOne } from 'typeorm';

@Entity({
  name: 'settings',
})
export class Settings {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @OneToOne(type => Manager, manager => manager.settings)
  user: Promise<Manager>;
}
