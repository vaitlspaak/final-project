import { Manager } from './manager.entity';
import { Funds } from './funds.entity';
import { Settings } from './settings.entity';
import { Order } from './order.entity';
import { Watchlist } from './watchlist.entity';
import { Role } from './role.entity';
import {
  Column,
  PrimaryGeneratedColumn,
  Entity,
  ManyToOne,
  OneToMany,
  OneToOne,
  JoinColumn,
} from 'typeorm';

@Entity({
  name: 'clients',
})
export class Client {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(type => Role, role => role.users, { eager: true })
  role: Role;

  @ManyToOne(type => Manager, manager => manager.clients, { eager: true })
  manager: Manager;

  @OneToMany(type => Watchlist, watchlist => watchlist.client)
  @JoinColumn()
  watchlist: Promise<Watchlist[]>;

  @OneToOne(type => Funds, funds => funds.client, {
    eager: true,
    cascade: true,
  })
  @JoinColumn()
  funds: Funds;

  @Column({ nullable: false })
  firstname: string;

  @Column({ nullable: false })
  lastname: string;

  @Column({ nullable: false })
  age: number;

  @Column({ nullable: false })
  address: string;

  @Column()
  dateregistered: Date;

  @Column()
  photoUrl: string;

  @Column({ unique: true })
  email: string;

  @OneToMany(type => Order, order => order.client)
  orders: Order[];
}
