import { Client } from './client.entity';
import { Company } from './company.entity';
import { Manager } from './manager.entity';

import {
  PrimaryGeneratedColumn,
  Entity,
  OneToMany,
  OneToOne,
  ManyToMany,
  JoinTable,
  ManyToOne,
  Column,
} from 'typeorm';

@Entity({
  name: 'watchlists',
})
export class Watchlist {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: false })
  name: string;

  @Column({ default: false })
  isdeleted: boolean;

  @ManyToOne(type => Client, client => client.watchlist, { eager: true })
  client: Client;

  @ManyToOne(type => Manager, manager => manager.watchlist)
  manager: Manager;

  @ManyToMany(type => Company, company => company.watchlists, {
    eager: true,
    cascade: true,
  })
  @JoinTable()
  companies: Company[];
}
