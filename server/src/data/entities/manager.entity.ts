import { Client } from './client.entity';
import { Funds } from './funds.entity';
import { Settings } from './settings.entity';
import { Order } from './order.entity';
import { Watchlist } from './watchlist.entity';
import { Role } from './role.entity';
import {
  Column,
  PrimaryGeneratedColumn,
  Entity,
  ManyToOne,
  OneToMany,
  OneToOne,
  JoinColumn,
} from 'typeorm';

@Entity({
  name: 'managers',
})
export class Manager {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(type => Role, role => role.users, { eager: true })
  role: Role;

  @OneToMany(type => Client, client => client.manager)
  clients: Promise<Client[]>;

  @OneToMany(type => Watchlist, watchlist => watchlist.manager)
  @JoinColumn()
  watchlist: Promise<Watchlist[]>;

  @OneToOne(type => Settings, settings => settings.user)
  settings: Promise<Settings>;

  @Column({ nullable: false })
  firstname: string;

  @Column({ nullable: false })
  lastname: string;

  @Column()
  dateregistered: Date;

  @Column({ unique: true })
  email: string;

  @Column({ default: '' })
  password: string;
}
