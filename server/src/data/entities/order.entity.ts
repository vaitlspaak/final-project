import { Client } from './client.entity';
import { Company } from './company.entity';
import { Status } from './status.entity';
import {
  Column,
  PrimaryGeneratedColumn,
  Entity,
  ManyToOne,
  OneToOne,
} from 'typeorm';

@Entity({
  name: 'orders',
})
export class Order {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(type => Client, client => client.orders, { eager: true })
  client: Client;

  @Column()
  opendate: Date;

  @Column({ nullable: true, default: null })
  closedate: Date;

  @ManyToOne(type => Company, company => company.orders, { eager: true })
  company: Company;

  @Column()
  price: number;

  @Column({ type: 'decimal' })
  closeprice: number;

  @Column()
  type: string;

  @Column()
  units: number;

  @Column()
  status: string;
}
