import { Client } from './client.entity';
import { Column, PrimaryGeneratedColumn, Entity, OneToOne } from 'typeorm';

@Entity({
  name: 'funds',
})
export class Funds {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @OneToOne(type => Client, client => client.funds)
  client: Promise<Client>;

  @Column({ default: 0 })
  currentamount: number;
}
