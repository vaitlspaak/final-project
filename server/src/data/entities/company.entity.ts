import { Sector } from './sector.entity';
import { Watchlist } from './watchlist.entity';
import { Order } from './order.entity';
import { Price } from './prices.entity';
import {
  Column,
  PrimaryGeneratedColumn,
  Entity,
  ManyToOne,
  OneToMany,
  ManyToMany,
} from 'typeorm';

@Entity({
  name: 'companies',
})
export class Company {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ default: '' })
  symbol: string;

  @Column({ default: '' })
  name: string;

  @Column({ default: '' })
  ipoyear: string;

  @Column({ default: '' })
  industry: string;

  @Column({ default: '' })
  summary: string;

  // @Column()
  // closedate: Date;

  @ManyToOne(type => Sector, sector => sector.company, { eager: true })
  sector: Sector;

  @OneToMany(type => Price, price => price.company)
  prices: Promise<Price[]>;

  @OneToMany(type => Order, order => order.company)
  orders: Promise<Order[]>;

  @ManyToMany(type => Watchlist, watchlist => watchlist.companies)
  watchlists: Promise<Watchlist[]>;
}
