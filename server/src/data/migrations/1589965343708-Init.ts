import { MigrationInterface, QueryRunner } from 'typeorm';

export class Init1589965343708 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE `orders` DROP FOREIGN KEY `FK_1457f286d91f271313fded23e53`');
        await queryRunner.query('ALTER TABLE `orders` DROP FOREIGN KEY `FK_b6fe899d5ca4a3f5925463990d1`');
        await queryRunner.query('ALTER TABLE `orders` CHANGE `opendate` `opendate` datetime NOT NULL');
        await queryRunner.query('ALTER TABLE `orders` CHANGE `closedate` `closedate` datetime NULL');
        await queryRunner.query('ALTER TABLE `orders` CHANGE `closeprice` `closeprice` decimal NOT NULL');
        await queryRunner.query('ALTER TABLE `orders` CHANGE `clientId` `clientId` varchar(255) NULL');
        await queryRunner.query('ALTER TABLE `orders` CHANGE `companyId` `companyId` varchar(255) NULL');
        await queryRunner.query('ALTER TABLE `prices` DROP FOREIGN KEY `FK_e4ac7a6865d8c92ef5137df5a41`');
        await queryRunner.query('ALTER TABLE `prices` CHANGE `opendate` `opendate` datetime NOT NULL');
        await queryRunner.query('ALTER TABLE `prices` CHANGE `companyId` `companyId` varchar(255) NULL');
        await queryRunner.query('ALTER TABLE `companies` DROP FOREIGN KEY `FK_cdbe33009892b1a590b35124516`');
        await queryRunner.query('ALTER TABLE `companies` CHANGE `sectorId` `sectorId` varchar(255) NULL');
        await queryRunner.query('ALTER TABLE `watchlists` DROP FOREIGN KEY `FK_32efa18ee75868ccadeac85b1b8`');
        await queryRunner.query('ALTER TABLE `watchlists` DROP FOREIGN KEY `FK_338633d11ba7528a66e9b8cd474`');
        await queryRunner.query('ALTER TABLE `watchlists` CHANGE `clientId` `clientId` varchar(255) NULL');
        await queryRunner.query('ALTER TABLE `watchlists` CHANGE `managerId` `managerId` varchar(255) NULL');
        await queryRunner.query('ALTER TABLE `managers` DROP FOREIGN KEY `FK_726c4c250e5c1265eeaa0bf137d`');
        await queryRunner.query('ALTER TABLE `managers` CHANGE `dateregistered` `dateregistered` datetime NOT NULL');
        await queryRunner.query('ALTER TABLE `managers` CHANGE `roleId` `roleId` varchar(255) NULL');
        await queryRunner.query('ALTER TABLE `clients` DROP FOREIGN KEY `FK_9b5ea79b60b4dca15f92f8f6942`');
        await queryRunner.query('ALTER TABLE `clients` DROP FOREIGN KEY `FK_b298c69fe5af01a26569338853f`');
        await queryRunner.query('ALTER TABLE `clients` DROP FOREIGN KEY `FK_b8e64b50749430c132cce33c38b`');
        await queryRunner.query('ALTER TABLE `clients` CHANGE `dateregistered` `dateregistered` datetime NOT NULL');
        await queryRunner.query('ALTER TABLE `clients` CHANGE `roleId` `roleId` varchar(255) NULL');
        await queryRunner.query('ALTER TABLE `clients` CHANGE `managerId` `managerId` varchar(255) NULL');
        await queryRunner.query('ALTER TABLE `clients` CHANGE `fundsId` `fundsId` varchar(255) NULL');
        await queryRunner.query('ALTER TABLE `orders` ADD CONSTRAINT `FK_1457f286d91f271313fded23e53` FOREIGN KEY (`clientId`) REFERENCES `clients`(`id`)');
        await queryRunner.query('ALTER TABLE `orders` ADD CONSTRAINT `FK_b6fe899d5ca4a3f5925463990d1` FOREIGN KEY (`companyId`) REFERENCES `companies`(`id`)');
        await queryRunner.query('ALTER TABLE `prices` ADD CONSTRAINT `FK_e4ac7a6865d8c92ef5137df5a41` FOREIGN KEY (`companyId`) REFERENCES `companies`(`id`)');
        await queryRunner.query('ALTER TABLE `companies` ADD CONSTRAINT `FK_cdbe33009892b1a590b35124516` FOREIGN KEY (`sectorId`) REFERENCES `sectors`(`id`)');
        await queryRunner.query('ALTER TABLE `watchlists` ADD CONSTRAINT `FK_32efa18ee75868ccadeac85b1b8` FOREIGN KEY (`clientId`) REFERENCES `clients`(`id`)');
        await queryRunner.query('ALTER TABLE `watchlists` ADD CONSTRAINT `FK_338633d11ba7528a66e9b8cd474` FOREIGN KEY (`managerId`) REFERENCES `managers`(`id`)');
        await queryRunner.query('ALTER TABLE `managers` ADD CONSTRAINT `FK_726c4c250e5c1265eeaa0bf137d` FOREIGN KEY (`roleId`) REFERENCES `roles`(`id`)');
        await queryRunner.query('ALTER TABLE `clients` ADD CONSTRAINT `FK_9b5ea79b60b4dca15f92f8f6942` FOREIGN KEY (`roleId`) REFERENCES `roles`(`id`)');
        await queryRunner.query('ALTER TABLE `clients` ADD CONSTRAINT `FK_b298c69fe5af01a26569338853f` FOREIGN KEY (`managerId`) REFERENCES `managers`(`id`)');
        await queryRunner.query('ALTER TABLE `clients` ADD CONSTRAINT `FK_b8e64b50749430c132cce33c38b` FOREIGN KEY (`fundsId`) REFERENCES `funds`(`id`)');
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('ALTER TABLE `clients` DROP FOREIGN KEY `FK_b8e64b50749430c132cce33c38b`');
        await queryRunner.query('ALTER TABLE `clients` DROP FOREIGN KEY `FK_b298c69fe5af01a26569338853f`');
        await queryRunner.query('ALTER TABLE `clients` DROP FOREIGN KEY `FK_9b5ea79b60b4dca15f92f8f6942`');
        await queryRunner.query('ALTER TABLE `managers` DROP FOREIGN KEY `FK_726c4c250e5c1265eeaa0bf137d`');
        await queryRunner.query('ALTER TABLE `watchlists` DROP FOREIGN KEY `FK_338633d11ba7528a66e9b8cd474`');
        await queryRunner.query('ALTER TABLE `watchlists` DROP FOREIGN KEY `FK_32efa18ee75868ccadeac85b1b8`');
        await queryRunner.query('ALTER TABLE `companies` DROP FOREIGN KEY `FK_cdbe33009892b1a590b35124516`');
        await queryRunner.query('ALTER TABLE `prices` DROP FOREIGN KEY `FK_e4ac7a6865d8c92ef5137df5a41`');
        await queryRunner.query('ALTER TABLE `orders` DROP FOREIGN KEY `FK_b6fe899d5ca4a3f5925463990d1`');
        await queryRunner.query('ALTER TABLE `orders` DROP FOREIGN KEY `FK_1457f286d91f271313fded23e53`');
        await queryRunner.query('ALTER TABLE `clients` CHANGE `fundsId` `fundsId` varchar(255) NULL DEFAULT \'NULL\'');
        await queryRunner.query('ALTER TABLE `clients` CHANGE `managerId` `managerId` varchar(255) NULL DEFAULT \'NULL\'');
        await queryRunner.query('ALTER TABLE `clients` CHANGE `roleId` `roleId` varchar(255) NULL DEFAULT \'NULL\'');
        await queryRunner.query('ALTER TABLE `clients` CHANGE `dateregistered` `dateregistered` datetime(0) NOT NULL');
        await queryRunner.query('ALTER TABLE `clients` ADD CONSTRAINT `FK_b8e64b50749430c132cce33c38b` FOREIGN KEY (`fundsId`) REFERENCES `funds`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT');
        await queryRunner.query('ALTER TABLE `clients` ADD CONSTRAINT `FK_b298c69fe5af01a26569338853f` FOREIGN KEY (`managerId`) REFERENCES `managers`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT');
        await queryRunner.query('ALTER TABLE `clients` ADD CONSTRAINT `FK_9b5ea79b60b4dca15f92f8f6942` FOREIGN KEY (`roleId`) REFERENCES `roles`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT');
        await queryRunner.query('ALTER TABLE `managers` CHANGE `roleId` `roleId` varchar(255) NULL DEFAULT \'NULL\'');
        await queryRunner.query('ALTER TABLE `managers` CHANGE `dateregistered` `dateregistered` datetime(0) NOT NULL');
        await queryRunner.query('ALTER TABLE `managers` ADD CONSTRAINT `FK_726c4c250e5c1265eeaa0bf137d` FOREIGN KEY (`roleId`) REFERENCES `roles`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT');
        await queryRunner.query('ALTER TABLE `watchlists` CHANGE `managerId` `managerId` varchar(255) NULL DEFAULT \'NULL\'');
        await queryRunner.query('ALTER TABLE `watchlists` CHANGE `clientId` `clientId` varchar(255) NULL DEFAULT \'NULL\'');
        await queryRunner.query('ALTER TABLE `watchlists` ADD CONSTRAINT `FK_338633d11ba7528a66e9b8cd474` FOREIGN KEY (`managerId`) REFERENCES `managers`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT');
        await queryRunner.query('ALTER TABLE `watchlists` ADD CONSTRAINT `FK_32efa18ee75868ccadeac85b1b8` FOREIGN KEY (`clientId`) REFERENCES `clients`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT');
        await queryRunner.query('ALTER TABLE `companies` CHANGE `sectorId` `sectorId` varchar(255) NULL DEFAULT \'NULL\'');
        await queryRunner.query('ALTER TABLE `companies` ADD CONSTRAINT `FK_cdbe33009892b1a590b35124516` FOREIGN KEY (`sectorId`) REFERENCES `sectors`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT');
        await queryRunner.query('ALTER TABLE `prices` CHANGE `companyId` `companyId` varchar(255) NULL DEFAULT \'NULL\'');
        await queryRunner.query('ALTER TABLE `prices` CHANGE `opendate` `opendate` datetime(0) NOT NULL');
        await queryRunner.query('ALTER TABLE `prices` ADD CONSTRAINT `FK_e4ac7a6865d8c92ef5137df5a41` FOREIGN KEY (`companyId`) REFERENCES `companies`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT');
        await queryRunner.query('ALTER TABLE `orders` CHANGE `companyId` `companyId` varchar(255) NULL DEFAULT \'NULL\'');
        await queryRunner.query('ALTER TABLE `orders` CHANGE `clientId` `clientId` varchar(255) NULL DEFAULT \'NULL\'');
        await queryRunner.query('ALTER TABLE `orders` CHANGE `closeprice` `closeprice` decimal(10,0) NOT NULL');
        await queryRunner.query('ALTER TABLE `orders` CHANGE `closedate` `closedate` datetime(0) NULL DEFAULT \'NULL\'');
        await queryRunner.query('ALTER TABLE `orders` CHANGE `opendate` `opendate` datetime(0) NOT NULL');
        await queryRunner.query('ALTER TABLE `orders` ADD CONSTRAINT `FK_b6fe899d5ca4a3f5925463990d1` FOREIGN KEY (`companyId`) REFERENCES `companies`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT');
        await queryRunner.query('ALTER TABLE `orders` ADD CONSTRAINT `FK_1457f286d91f271313fded23e53` FOREIGN KEY (`clientId`) REFERENCES `clients`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT');
    }

}
