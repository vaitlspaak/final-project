import { ServerErrorComponent } from './common/server-error/server-error.component';
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './common/page-not-found/page-not-found.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },

  { path: 'admin', loadChildren: './admin/admin.module#AdminModule' },
  { path: 'manager', loadChildren: './manager/manager.module#ManagerModule' },

  { path: 'login', component: LoginComponent },
  { path: 'not-found', component: PageNotFoundComponent },
  { path: 'server-error', component: ServerErrorComponent },
  { path: '**', redirectTo: 'not-found', pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
      anchorScrolling: 'enabled'
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
