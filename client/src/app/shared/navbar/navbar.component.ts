import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from './../../core/auth.service';
import { Component, OnInit, Renderer2, AfterContentInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { StorageService } from './../../core/storage.service';
import { ClientAddEditModel } from './../models/clientAddEdit.model';
import { ClientsService } from './../../core/client.service';
import { CommunicationService } from './../../core/communication.service';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, AfterContentInit {
  private loginSubscription: Subscription;
  private clientCurrentlyManaged: ClientAddEditModel = new ClientAddEditModel();
  public isLoggedIn: boolean;
  private hasClientId: string;
  private showExitButton: boolean;
  private showProfileContainer: boolean;
  public userEmail = '';
  public userRole = '';

  constructor(
    private readonly authService: AuthService,
    private readonly notificator: ToastrService,
    private readonly router: Router,
    private readonly jwtHelperService: JwtHelperService,
    private readonly storageService: StorageService,
    private readonly clientService: ClientsService,
    private readonly communicationService: CommunicationService,
    private renderer: Renderer2
  ) {}

  public ngOnInit(): void {
    this.authService.isLoggedIn$.subscribe(
      (isLogged) => (this.isLoggedIn = isLogged)
    );
    // this.userEmail = this.jwtHelperService.decodeToken().email;
    // this.userRole = this.jwtHelperService.decodeToken().role;
    this.hasClientId = this.storageService.getItem('clientId');
    this.communicationService.isManagingSubject$.subscribe(
      (clientInitialized) => {
        if (
          this.storageService.getItem('clientId') !== null &&
          clientInitialized
        ) {
          this.clientService
            .getClientById(this.storageService.getItem('clientId'))
            .subscribe((client) => {
              this.clientCurrentlyManaged = client;
              this.communicationService.isExitButtonvsible$.next(true);
              this.communicationService.isManagingSubject$.next(true);
            });
        }
      }
    );
  }

  ngAfterContentInit(): void {
    this.communicationService.isExitButtonvsible$.subscribe((isVisible) => {
      if (isVisible) {
        this.renderer.addClass(document.body, 'backgroundClient');
      } else {
        this.renderer.removeClass(document.body, 'backgroundClient');
      }
      this.showExitButton = isVisible;
      this.showProfileContainer = isVisible;
    });

    this.communicationService.isManagingSubject$.subscribe((isVisible) => {
      this.showProfileContainer = isVisible;
    });
  }

  exitClientMode() {
    this.communicationService.isExitButtonvsible$.next(false);
    this.communicationService.isManagingSubject$.next(false);

    this.storageService.removeItem('clientId');
    this.router.navigate(['/manager/home']);
    this.renderer.removeClass(document.body, 'backgroundClient');
  }
  logout() {
    this.authService.logoutUser();
    this.router.navigate(['/login']);
    this.notificator.success(
      `You've been successfully logged out of the system `,
      'Logged out'
    );
  }
}
