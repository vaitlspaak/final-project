import { ClosePositionModel } from './../models/closePosition.model';
import { MarketService } from './../../core/market.service';
import { ModalService } from './../../core/modal.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-confimation-modal',
  templateUrl: './confimation-modal.component.html'
})
export class ConfimationModalComponent implements OnInit {
  @Input()
  id: string;
  @Input()
  private companyName: string;
  @Input()
  private units: number;
  @Input()
  private price: number;
  @Input()
  private closePrice: number;

  constructor(
    private modalService: ModalService,
    private marketService: MarketService
  ) {}

  ngOnInit() {}

  public closeTrade(id: string, closePrice: number): void {
    const closePosition: ClosePositionModel = { id, closePrice };

    this.marketService.closeTrade(closePosition);
  }
  public closeModal(): void {
    this.modalService.closeModal();
  }
}
