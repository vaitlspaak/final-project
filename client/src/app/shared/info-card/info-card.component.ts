import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-info-card',
  templateUrl: './info-card.component.html',
  styleUrls: ['./info-card.component.css']
})
export class InfoCardComponent implements OnInit {
  @Input()
  private infoText: string;
  @Input()
  private infoTextString: string;
  @Input()
  private numbersText: number;
  @Input()
  private iconStyle: string;
  @Input()
  private fieldType: string;

  constructor() { }

  ngOnInit() { }

  calculateColor(number: number, fieldType: string) {

    if (number > 100 && fieldType !== 'percent') {
      return 'green';
    } else if (number < 100 && fieldType !== 'percent') {
      return 'red';
    } else if (number > 0 && fieldType === 'percent') {
      return 'green';
    } else {
      return 'red';
    }
  }
}
