export class PriceModel {
  id: string;
  opendate: Date;
  startprice: number;

  endprice: number;

  highprice: number;

  lowprice: number;
}
