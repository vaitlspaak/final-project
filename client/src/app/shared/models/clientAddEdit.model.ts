import { FundsModel } from './funds.model';
import { ManagerOrAdminAddEditModel } from './managerOrAdmin.model';
export class ClientAddEditModel {
  firstname: string;
  lastname: string;
  address: string;
  age: number;
  email: string;
  funds: FundsModel;
  manager: ManagerOrAdminAddEditModel;
}
