export class GetClientFinancialDataModel {
  currentOverallBalance: number;
  currentlyInvestedMoney: number;
  moneyAvailableForInvestment: number;
}
