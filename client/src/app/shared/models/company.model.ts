import { SectorModel } from './sector.model';

export class CompanyModel {
  id: string;
  name: string;
  symbol: string;
  sector: SectorModel;
  ipoyear: string;
  industry: string;
  summary: string;
  currentprice: number;
}
