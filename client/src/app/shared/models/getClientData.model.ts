import { FundsModel } from './funds.model';
export class GetClientDataModel {
  firstname: string;
  lastname: string;
  address: string;
  age: number;
  email: string;
  funds: FundsModel;
}
