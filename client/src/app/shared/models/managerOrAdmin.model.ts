import { Role } from './role.model';
export class ManagerOrAdminAddEditModel {
  role: Role;

  firstname: string;

  lastname: string;

  email: string;

  password: string;
}
