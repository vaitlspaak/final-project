export class PriceRequestModel {
  id: string;
  startdate: Date;
  enddate: Date;
  lastN: number;
}
