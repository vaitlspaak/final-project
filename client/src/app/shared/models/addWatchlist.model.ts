export class AddWatchlistModel {
  name: string;
  ownerId: string;
  managerId: string;
}
