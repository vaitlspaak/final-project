export class GetClosedPositionModel {
  companyName: string;
  units: number;
  price: number;
  closePrice: number;
  invested: number;
  openTime: string;
  closeTime: string;

  profitlossmoney: number;
  profitlosspercentage: number;
}
