export class PositionDataModel {
  price: string;
  type: string;
  units: string;
  clientId: string;
  companyId: string;
  status: string;
}
