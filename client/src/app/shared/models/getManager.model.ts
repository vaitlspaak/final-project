import { Role } from './role.model';
export class GetManagerModel {
  firstname: string;

  lastname: string;

  email: string;

  dateRegistered: string;
  role: Role;
}
