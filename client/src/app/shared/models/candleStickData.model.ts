export class CandleStickDataModel {
  data: {
    x: Date;
    y: number[];
  };
}
