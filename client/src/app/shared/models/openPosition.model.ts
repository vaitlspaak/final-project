export class OpenPositionModel {
  id: string;
  companyName: string;
  units: number;
  price: number;
  currentprice: number;
  type: string;
  invested: number;
  profitlossmoney: number;
  profitlosspercentage: number;
}
