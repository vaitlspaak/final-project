import { ClientsFilterPipe } from './../pipes/clients-filter.pipe';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { ConfimationModalComponent } from './confimation-modal/confimation-modal.component';
import { FormsModule } from '@angular/forms';
import { InfoCardComponent } from './info-card/info-card.component';

@NgModule({
  declarations: [
    NavbarComponent,
    NavbarComponent,
    ConfimationModalComponent,
    InfoCardComponent,
    ClientsFilterPipe
  ],
  imports: [CommonModule, RouterModule, FormsModule],
  exports: [
    CommonModule,
    RouterModule,
    NavbarComponent,
    ConfimationModalComponent,
    InfoCardComponent,
    ClientsFilterPipe
  ]
})
export class SharedModule {}
