import { Component, OnInit, Input } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { StorageService } from './../../core/storage.service';

@Component({
  selector: 'app-manager-trade-button',
  templateUrl: './manager-trade-button.html'
})
export class ManagerTradeButtonComponent implements OnInit {
  @Input()
  private id: string;

  constructor(private storageService: StorageService) {}

  public params: any;
    ngOnInit() {}

    agInit(params: any): void {
    }
  refresh(): boolean {
    return false;
  }

  setToken(id: string) {
    this.storageService.setItem('clientId', id);
  }
}
