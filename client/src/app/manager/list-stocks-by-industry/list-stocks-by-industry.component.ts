import { SectorModel } from './../../shared/models/sector.model';
import { SectorService } from './../../core/sector.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-stocks-by-industry',
  templateUrl: './list-stocks-by-industry.component.html',
  styleUrls: ['./list-stocks-by-industry.component.css']
})
export class ListStocksByIndustryComponent implements OnInit {
  private sectorsData: SectorModel[];
  private paginationPageSize: number;
  private pagination: boolean;
  constructor(
    private readonly sectorService: SectorService,
    private readonly router: Router
  ) {}

  ngOnInit() {
    // Gets all companies and sectors from the DB
    this.sectorService.getAllSectors().subscribe((sector) => {
      this.sectorsData = sector;
    });

    // Sets how many rows to be displayed & if there should be a pagination
    // Settings for ListStocksByIndustry
    this.paginationPageSize = 8;
    this.pagination = true;
  }
}
