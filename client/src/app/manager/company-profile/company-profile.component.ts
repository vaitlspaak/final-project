import { StorageService } from 'src/app/core/storage.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TradeModalComponent } from './../trade-modal/trade-modal.component';
import { PriceModel } from './../../shared/models/price.model';
import { PriceService } from './../../core/price.service';
import { PriceRequestModel } from './../../shared/models/price-request.model';
import { StocksService } from './../../core/stocks.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-company-profile',
  templateUrl: './company-profile.component.html',
  styleUrls: ['./company-profile.component.css']
})
export class CompanyProfileComponent implements OnInit {
  private symbol: string;
  private companyName: string;
  private industry: string;
  private sector: string;
  private ipoYear: string;

  private companyId: string;
  private clientId: string;

  private prices: PriceModel[];

  private buyPrice: number;
  private sellPrice: number;

  @ViewChild(TradeModalComponent) modal: ElementRef;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly stocksService: StocksService,
    private readonly priceService: PriceService,
    private modalService: NgbModal,
    private storageService: StorageService
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.symbol = params['symbol'];
    });

    this.clientId = this.storageService.getItem('clientId');
    this.stocksService.getCompanyDataBySymbol(this.symbol).subscribe(
      (dbCompany) => {
        this.ipoYear = dbCompany.ipoyear;
        this.companyName = dbCompany.name;
        this.industry = dbCompany.industry;
        this.sector = dbCompany.sector.name;
        this.companyId = dbCompany.id;

        this.priceService.getCompanyPrices(this.symbol).subscribe(
          (prices) => {
            this.prices = prices;
            this.buyPrice = this.prices[0].endprice;
            this.sellPrice = this.buyPrice - 0.005 * this.buyPrice;
          },
          () => {}
        );
      },
      () => {}
    );
  }

  onTrade(content) {
    this.modalService.open(content);
  }
}
