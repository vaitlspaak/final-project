import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list-industry-stocks',
  templateUrl: './list-industry-stocks.component.html',
  styleUrls: ['./list-industry-stocks.component.css']
})
export class ListIndustryStocksComponent implements OnInit {
  @Input()
  private sectorId: string;
  @Input()
  private sectorTitle: string;
  @Input()
  private paginationPageSize: number;
  @Input()
  private pagination: boolean;

  private id: string;

  constructor(private readonly route: ActivatedRoute) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.id = params['id'];
    });

    this.sectorId = this.id;
    this.paginationPageSize = 50;
    this.pagination = true;
  }
}
