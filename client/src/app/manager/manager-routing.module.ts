import { DashboardComponent } from './dashboard/dashboard.component';
import { ClientProfileComponent } from './client-profile/client-profile.component';
import { ClientClosedPositionsComponent } from './client-closed-positions/client-closed-positions.component';
import { ClientOpenPositionsComponent } from './client-open-positions/client-open-positions.component';
import { WatchlistEditComponent } from './watchlist-edit/watchlist-edit.component';
import { NgModule } from '@angular/core';
import { ListStocksByIndustryComponent } from './list-stocks-by-industry/list-stocks-by-industry.component';
import { ManagerClientsListComponent } from './manager-clients-list/manager-clients-list.component';
import { AuthGuardService } from '../core/auth-guard.service';
import { Routes, RouterModule } from '@angular/router';
import { ListIndustryStocksComponent } from './list-industry-stocks/list-industry-stocks.component';
import { CompanyProfileComponent } from './company-profile/company-profile.component';
import { ManagerTradeButtonComponent } from './manager-trade-button/manager-trade-button';

const routes: Routes = [
  {
    path: 'home',
    component: DashboardComponent,
    canActivate: [AuthGuardService],
    pathMatch: 'full'
  },
  {
    path: 'clients/list',
    component: ManagerClientsListComponent,
    canActivate: [AuthGuardService],
    pathMatch: 'full'
  },
  {
    path: 'clients/:id/profile',
    component: ClientProfileComponent,
    canActivate: [AuthGuardService],
    pathMatch: 'full'
  },
  {
    path: 'stocks',
    component: ListStocksByIndustryComponent,
    canActivate: [AuthGuardService],
    pathMatch: 'full'
  },
  {
    path: 'stocks/sector/:id',
    component: ListIndustryStocksComponent,
    canActivate: [AuthGuardService],

    pathMatch: 'full'
  },
  {
    path: 'stocks/:symbol',
    component: CompanyProfileComponent,
    canActivate: [AuthGuardService],
    pathMatch: 'full'
  },
  {
    path: 'trade/:symbol',
    component: ManagerTradeButtonComponent,
    canActivate: [AuthGuardService],
    pathMatch: 'full'
  },
  {
    path: 'watchlist/add',
    component: WatchlistEditComponent,
    canActivate: [AuthGuardService],
    pathMatch: 'full'
  },
  {
    path: 'open',
    component: ClientOpenPositionsComponent,
    canActivate: [AuthGuardService],
    pathMatch: 'full'
  },
  {
    path: 'closed',
    component: ClientClosedPositionsComponent,
    canActivate: [AuthGuardService],
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagerRoutingModule {}
