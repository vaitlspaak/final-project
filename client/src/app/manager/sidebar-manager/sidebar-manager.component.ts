import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-sidebar-manager',
  templateUrl: './sidebar-manager.component.html',
  styleUrls: ['../../../assets/css/sidebar-menu.css']
})
export class ManagerSidebarComponent implements OnInit {
  constructor(
    private router: Router,
    private authService: AuthService,
    private notificator: ToastrService
  ) {}

  ngOnInit() {}

  public homeRoute(): void {
    this.router.navigateByUrl('manager/home');
  }

  public managerClientsProfileRoute(): void {
    this.router.navigateByUrl('manager/clients/profile');
  }

  public managerClientsListRoute(): void {
    this.router.navigateByUrl('manager/clients/list');
  }

  public managerClientsElementRoute(): void {
    this.router.navigateByUrl('manager/clients/element');
  }

  public ListStocksByIndustryRoute(): void {
    this.router.navigateByUrl('manager/stocks');
  }

  public managerWatchlistAddRoute(): void {
    this.router.navigateByUrl('manager/watchlist/add');
  }

  public openPositionsRoute(): void {
    this.router.navigateByUrl('manager/open');
  }

  public closedPositionsRoute(): void {
    this.router.navigateByUrl('manager/closed');
  }

  public logout() {
    this.authService.logoutUser();
    this.router.navigate(['login']);
    this.notificator.success(
      `You've been successfully logged out of the system `,
      'Logged out'
    );
  }
}
