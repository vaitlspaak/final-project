import { ModalService } from './../../core/modal.service';
import { MarketService } from './../../core/market.service';
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl
} from '@angular/forms';
import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-trade-modal',
  templateUrl: './trade-modal.component.html',
  styleUrls: ['./trade-modal.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TradeModalComponent implements OnInit {
  @Input()
  private sellPrice: number;
  @Input()
  private buyPrice: number;
  @Input()
  private companyName: string;
  @Input()
  private companyId: string;
  @Input()
  private clientId: string;
  private closeResult: string;
  private availableFunds: number;
  private openPosition: FormGroup;
  private totalSum: string;

  constructor(
    config: NgbModalConfig,
    private fb: FormBuilder,
    private marketService: MarketService,
    private modalService: ModalService
  ) {
    config.keyboard = false;
  }

  ngOnInit() {
    this.openPosition = this.fb.group({
      price: ['', Validators.required],
      type: ['', Validators.required],
      units: ['', Validators.required],
      clientId: [`${this.clientId}`, Validators.required],
      companyId: [`${this.companyId}`, Validators.required],
      status: ['open', Validators.required]
    });

    this.marketService
      .getUsersAvailableFunds(`${this.clientId}`)
      .subscribe((funds) => {
        this.availableFunds = funds;
      });
  }

  public numberValidator(
    control: AbstractControl
  ): { [key: string]: any } | null {
    const valid = /^\d+$/.test(control.value);
    return valid
      ? null
      : { invalidNumber: { valid: false, value: control.value } };
  }

  public tradeMarket() {
    return this.marketService.openTrade(this.openPosition.value);
  }

  public calculateTotal(): void {
    setTimeout(() => {}, 1000);

    if (this.openPosition.value.type === 'buy') {
      const amountToBeInvested = +(
        this.buyPrice * this.openPosition.value.units
      ).toFixed(2);

      this.totalSum = `Total amount to be invested: ${amountToBeInvested}`;

      if (amountToBeInvested > this.availableFunds) {
        this.totalSum = `The maximum number of stocks you can purchase is: ${Math.floor(
          this.availableFunds / this.buyPrice
        )} units`;

        this.openPosition.patchValue({
          units: Math.floor(this.availableFunds / this.buyPrice)
        });
      }

      this.openPosition.patchValue({
        price: this.buyPrice
      });
    } else if (this.openPosition.value.type === 'sell') {
      const amountToBeInvested = +(
        this.sellPrice * this.openPosition.value.units
      ).toFixed(2);

      this.totalSum = `Total amount to be invested: ${amountToBeInvested}`;

      if (amountToBeInvested > this.availableFunds) {
        this.totalSum = `The maximum number of stocks you can sell is: ${Math.floor(
          this.availableFunds / this.sellPrice
        )} units`;

        this.openPosition.patchValue({
          units: Math.floor(this.availableFunds / this.sellPrice)
        });
      }

      this.openPosition.patchValue({
        price: +this.sellPrice
      });
    }
  }

  public closeModal(): void {
    this.modalService.closeModal();
  }
}
