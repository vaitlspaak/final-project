import { WatchlistService } from './../../core/watchlist.service';
import { ClientAddEditModel } from './../../shared/models/clientAddEdit.model';
import { ManagerAdminService } from './../../core/manager-admin.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-watchlist-edit',
  templateUrl: './watchlist-edit.component.html',
  styleUrls: ['./watchlist-edit.component.css']
})
export class WatchlistEditComponent implements OnInit {
  private watchlistForm: FormGroup;
  private managerId: string;
  private clientsList: ClientAddEditModel[];

  constructor(
    private readonly fb: FormBuilder,
    private readonly jwtHelperService: JwtHelperService,
    private readonly managerService: ManagerAdminService,
    private readonly watchlistService: WatchlistService
  ) {}

  ngOnInit() {
    this.managerId = this.jwtHelperService.decodeToken().id;
    this.watchlistForm = this.fb.group({
      name: ['', Validators.required],
      ownerId: [
        'Add this watchlist to my personal watchlists',
        Validators.required
      ],
      managerId: [`${this.managerId}`, Validators.required],
      isDeleted: [0, Validators.required]
    });

    this.managerService
      .getClientByManagerId(this.managerId)
      .subscribe((clients) => {
        this.clientsList = clients;
      });
  }

  addWatchlist() {

    if (this.watchlistForm.value.ownerId === this.managerId) {
      this.watchlistForm.patchValue({ ownerId: null });
    }
    return this.watchlistService.addWatchList(this.watchlistForm.value);
  }
}
