import { Component, OnInit } from '@angular/core';
import { Client } from '../../../../../server/src/data/entities/client.entity';
import { ClientsListService } from 'src/app/core/clients-list.service';
import { StorageService } from './../../core/storage.service';

@Component({
  selector: 'app-manager-clients-list',
  templateUrl: './manager-clients-list.component.html',
  styleUrls: ['./manager-clients-list.component.css']
})
export class ManagerClientsListComponent implements OnInit {
  private clientId: string;
  private clients: Client[];
  private columns: string[];
  private searchTerm: string;

  constructor(
    private clientsListService: ClientsListService,
    private storageService: StorageService
  ) {}

  ngOnInit() {
    // this.storageService.removeItem('clientId');

    this.clientsListService.getClients().subscribe((client) => {
      this.clients = client;
    });
    this.columns = this.clientsListService.getColumnsForTrade();
  }
}
