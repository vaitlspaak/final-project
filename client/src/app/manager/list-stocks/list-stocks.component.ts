import { CompanyModel } from './../../shared/models/company.model';
import { ViewCompanyButtonComponent } from './../view-company-button/view-company-button.component';
import { StocksService } from './../../core/stocks.service';
import { Component, OnInit, Input } from '@angular/core';
import 'rxjs/add/observable/interval';

@Component({
  selector: 'app-list-stocks',
  templateUrl: './list-stocks.component.html',
  styleUrls: ['./list-stocks.component.css']
})
export class ListStocksComponent implements OnInit {
  @Input()
  private sectorId: string;
  @Input()
  private paginationPageSize: number;
  @Input()
  private pagination: boolean;
  private sectorTitle: string;
  private columnDefs: Object[];
  private rowData: CompanyModel[];
  private gridApi;
  private frameworkComponents: Object;
  private sub: any;
  constructor(private readonly stocksService: StocksService) {}

  ngOnInit() {
    this.frameworkComponents = {
      viewCompanyButton: ViewCompanyButtonComponent
    };
    this.columnDefs = [
      {
        headerName: 'Symbol',
        field: 'symbol',
        sortable: true,
        filter: true,
        resizable: true
      },
      {
        headerName: 'Name',
        field: 'name',
        sortable: true,
        filter: 'agTextColumnFilter',
        resizable: true
      },
      {
        headerName: 'Industry',
        field: 'industry',
        sortable: true,
        filter: true,
        resizable: true
      },
      {
        headerName: 'Current price',
        field: 'currentprice',
        sortable: true,
        filter: true,
        resizable: true
      },
      {
        headerName: 'Details',
        field: 'details',
        sortable: false,
        cellRenderer: 'viewCompanyButton',
        filter: true,
        resizable: true
      }
    ];

    this.stocksService
      .getAllCompaniesBySector(this.sectorId)
      .subscribe((company) => {
        this.rowData = company;
        this.sectorTitle = this.rowData[0].sector.name;

        this.rowData.map((data) => {
          data.currentprice = +(Math.random() * 500).toFixed(2);
        });
      });
  }

  public getRowStyle = function(params) {
    if (params.node.rowIndex % 2 === 0) {
      return { background: '#00000021' };
    }
  };

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
  }
}
