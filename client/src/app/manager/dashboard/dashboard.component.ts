import { GetClientIdModel } from './../../shared/models/getClientId.model';
import { MarketService } from './../../core/market.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  private clientIds: GetClientIdModel[];
  private pagination: boolean;
  private paginationPageSize: number;
  constructor(private marketService: MarketService) {}

  ngOnInit() {
    this.marketService
      .getClientsWithOpenedPositions()
      .subscribe((clientIds) => {
        this.clientIds = clientIds;
      });
    this.pagination = true;
    this.paginationPageSize = 10;
  }
}
