import { Component, OnInit, Input } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'app-view-company-button',
  templateUrl: './view-company-button.component.html',
  styleUrls: ['./view-company-button.component.css']
})
export class ViewCompanyButtonComponent
  implements OnInit, ICellRendererAngularComp {
  private symbol: string;
  constructor() {}
  public params: any;

  ngOnInit() {}

  agInit(params: any): void {
    this.params = params;
    this.symbol = this.params.data.symbol;
  }

  refresh(): boolean {
    return false;
  }
}
