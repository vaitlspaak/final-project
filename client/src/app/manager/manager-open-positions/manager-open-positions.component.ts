import { ClosePositionButtonComponent } from '../close-position-button/close-position-button.component';
import { OpenPositionModel } from './../../shared/models/openPosition.model';
import { ClientsService } from './../../core/client.service';
import { AgGridService } from 'src/app/core/ag-grid.service';
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-manager-open-positions',
  templateUrl: './manager-open-positions.component.html',
  styleUrls: ['./manager-open-positions.component.css']
})
export class ManagerOpenPositionsComponent implements OnInit {
  private columnDefs: Object[];
  private rowData: OpenPositionModel[];
  private gridApi;
  private frameworkComponents: Object;
  private cardBoxInfoText: string;
  private clientName: string;
  private clientEmail: string;
  @Input()
  private clientId: string;
  @Input()
  private paginationPageSize: number;
  @Input()
  private pagination: boolean;
  private profitLossOpenedPositions: number;

  constructor(
    private clientService: ClientsService,
    private rendererSymbol: AgGridService,
    private router: Router
  ) {}

  ngOnInit() {
    this.cardBoxInfoText = 'Current Profit/Loss($)';

    this.clientService.getClientById(this.clientId).subscribe((client) => {
      this.clientName = `${client.firstname} ${client.lastname}`;
      this.clientEmail = `${client.email}`;
    });

    this.columnDefs = [
      {
        headerName: 'Company Name(Symbol)',
        field: 'companyName',
        sortable: true,
        resizable: true,
        filter: true
      },
      {
        headerName: 'Units',
        field: 'units',
        sortable: true,
        filter: true,
        width: 140,
        // align: 'center',
        resizable: true,
        cellStyle: {
          'text-align': 'center'
        }
      },
      {
        headerName: 'Open',
        field: 'price',
        sortable: false,
        filter: true,
        width: 140,
        resizable: true,
        cellRenderer: this.rendererSymbol.currencyUSDRenderer,
        cellStyle: {
          'text-align': 'center'
        }
      },
      {
        headerName: 'Invested',
        field: 'invested',
        sortable: true,
        filter: true,
        width: 140,
        resizable: true,
        cellRenderer: this.rendererSymbol.currencyUSDRenderer,
        cellStyle: {
          'text-align': 'center'
        }
      },
      {
        headerName: 'Current price',
        field: 'currentprice',
        sortable: true,
        filter: true,
        width: 140,
        // align: 'center',
        resizable: true,
        cellRenderer: this.rendererSymbol.currencyUSDRenderer,
        cellStyle: {
          'text-align': 'center'
        }
      },
      {
        headerName: 'P/L ($)',
        field: 'profitlossmoney',
        sortable: true,
        filter: true,
        resizable: true,
        cellRenderer: this.rendererSymbol.currencyUSDRenderer,
        cellStyle: function(params) {
          if (params.value >= 0) {
            return {
              color: 'green',
              backgroundColor: 'rgba(255, 255, 255, 0.7)'
            };
          } else {
            return {
              color: 'red',
              backgroundColor: 'rgba(255, 255, 255, 0.7)'
            };
          }
        }
      },
      {
        headerName: 'P/L (%)',
        field: 'profitlosspercentage',
        sortable: true,
        filter: true,
        resizable: true,
        cellRenderer: this.rendererSymbol.currencyPercentRenderer
      }
    ];

    this.clientService
      .getClientOpenPositions(this.clientId)
      .subscribe((positions) => {
        this.rowData = positions;
        this.updateDynamicData(this.rowData);

        setInterval(() => {
          {
            this.updateDynamicData(this.rowData);
          }
        }, 3000);
      });
  }

  public getRowStyle = function(params) {
    if (params.node.rowIndex % 2 === 0) {
      return { background: '#00000021' };
    }
  };

  private updateDynamicData(rowData: OpenPositionModel[]): void {
    this.profitLossOpenedPositions = 0;
    rowData.map((order) => {
      order.currentprice = +(Math.random() * 500).toFixed(2);

      order.profitlossmoney = +(
        order.currentprice * order.units -
        order.price * order.units
      ).toFixed(2);

      this.profitLossOpenedPositions += order.profitlossmoney;

      const profitLossPercentageCalculation = +(
        ((order.currentprice * order.units) / (order.price * order.units)) *
          100 -
        100
      ).toFixed(2);

      order.profitlosspercentage = profitLossPercentageCalculation;

      this.gridApi.refreshCells({
        field: ['currentprice', 'profitlossmoney']
      });
      return order;
    });
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
  }

  goToProfile(clientId: string): void {
    this.router.navigate([`/manager/clients/${clientId}/profile`]);
  }
}
