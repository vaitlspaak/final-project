import { CommunicationService } from './../../core/communication.service';
import { GetClientFinancialDataModel } from './../../shared/models/getClientFinancialData.model';
import { GetClientDataModel } from './../../shared/models/getClientData.model';
import { ClientsService } from './../../core/client.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/core/storage.service';

@Component({
  selector: 'app-client-profile',
  templateUrl: './client-profile.component.html',
  styleUrls: ['./client-profile.component.css']
})
export class ClientProfileComponent implements OnInit {
  private clientId: string;
  private client: GetClientDataModel;
  private clientName: string;
  private clientAge: number;
  private clientEmail: string;
  private clientAddress: string;
  private clientDepositedFunds: number;
  private routerLink: string;
  private profitLossMoney: number;
  private profitLossPercentage: number;
  private currentlyInvestedMoney: number;
  private moneyAvailableForInvestment: number;
  private currentOverallBalance: number;
  private finData: GetClientFinancialDataModel;

  constructor(
    private activatedRoute: ActivatedRoute,
    private clientService: ClientsService,
    private communicationService: CommunicationService,
    private storageService: StorageService
  ) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe((params) => {
      this.clientId = params['id'];
      this.storageService.setItem('clientId', this.clientId);

      this.clientService
        .getClientById(this.clientId)
        .subscribe((clientData) => {
          this.client = clientData;
          this.clientName = `${this.client.firstname} ${this.client.lastname}`;
          this.clientEmail = this.client.email;
          this.clientAddress = this.client.address;
          this.clientAge = this.client.age;
          this.clientDepositedFunds = +this.client.funds.currentamount;
          // this.communicationService.isManagingSubject$.next(true);
          this.communicationService.isExitButtonvsible$.next(true);
        });

      this.clientService
        .getClientFinancialInfo(this.clientId)
        .subscribe((finData) => {
          this.finData = finData;
          this.currentOverallBalance = finData.currentOverallBalance;
          this.profitLossMoney =
            +this.finData.currentOverallBalance - this.clientDepositedFunds;
          this.profitLossPercentage =
            +this.finData.currentOverallBalance / this.clientDepositedFunds - 1;

          this.currentlyInvestedMoney = this.finData.currentlyInvestedMoney;
          this.moneyAvailableForInvestment = this.finData.moneyAvailableForInvestment;
        });
    });
  }
}
