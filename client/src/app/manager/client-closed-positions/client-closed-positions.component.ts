import { GetClosedPositionModel } from './../../shared/models/getClosedPosition.model';
import { ClosePositionButtonComponent } from '../close-position-button/close-position-button.component';
import { ClientsService } from './../../core/client.service';
import { AgGridService } from 'src/app/core/ag-grid.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-client-closed-positions',
  templateUrl: './client-closed-positions.component.html',
  styleUrls: ['./client-closed-positions.component.css']
})
export class ClientClosedPositionsComponent implements OnInit {
  private columnDefs: Object[];
  private rowData: GetClosedPositionModel[];
  private gridApi;
  private frameworkComponents: Object;
  @Input()
  private clientId: string;

  constructor(
    private clientService: ClientsService,
    private rendererSymbol: AgGridService
    ) {}

  ngOnInit() {
    this.frameworkComponents = {
      closePositionButton: ClosePositionButtonComponent
    };
    this.columnDefs = [
      {
        headerName: 'Company Name(Symbol)',
        field: 'companyName',
        sortable: true,
        filter: true,
        resizable: true
      },
      {
        headerName: 'Units',
        field: 'units',
        sortable: true,
        filter: true,
        width: 140,
        // align: 'center',
        resizable: true,
        cellStyle: {
          'text-align': 'center'
        }
      },
      {
        headerName: 'Open',
        field: 'openPrice',
        sortable: false,
        filter: true,
        width: 140,
        resizable: true,
        cellRenderer: this.rendererSymbol.currencyUSDRenderer,
        cellStyle: {
          'text-align': 'center'
        }
      },
      {
        headerName: 'Open time',
        field: 'openTime',
        sortable: false,
        filter: true,
        width: 140,
        resizable: true,
        cellStyle: {
          'text-align': 'center'
        }
      },
      {
        headerName: 'Close',
        field: 'closePrice',
        sortable: true,
        filter: true,
        width: 140,
        resizable: true,
        cellRenderer: this.rendererSymbol.currencyUSDRenderer,
        // align: 'center',
        cellStyle: {
          'text-align': 'center'
        }
      },
      {
        headerName: 'Close time',
        field: 'closeTime',
        sortable: false,
        filter: true,
        width: 140,
        resizable: true,
        cellStyle: {
          'text-align': 'center'
        }
      },
      {
        headerName: 'P/L ($)',
        field: 'profitlossmoney',
        sortable: true,
        filter: true,
        resizable: true,
        cellRenderer: this.rendererSymbol.currencyUSDRenderer,
        cellStyle: function(params) {
          if (params.value >= 0) {
            return { color: 'green', backgroundColor: 'rgba(255, 255, 255, 0.7)' };
          } else {
            return { color: 'red', backgroundColor: 'rgba(255, 255, 255, 0.7)' };
          }
        }
      },
      {
        headerName: 'P/L (%)',
        field: 'profitlosspercentage',
        sortable: true,
        filter: true,
        resizable: true,
        cellRenderer: this.rendererSymbol.currencyPercentRenderer
      }
    ];

    this.clientService
      .getClientClosedPositions(this.clientId)
      .subscribe((positions) => {
        this.rowData = positions;
      });
  }

  public getRowStyle = function (params) {
    if (params.node.rowIndex % 2 === 0) {
      return { background: '#00000021' };
    }
  };

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
  }
}
