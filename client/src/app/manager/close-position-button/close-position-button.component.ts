import { ConfimationModalComponent } from './../../shared/confimation-modal/confimation-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, Input } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'app-close-position-button',
  templateUrl: './close-position-button.component.html'
})
export class ClosePositionButtonComponent
  implements OnInit, ICellRendererAngularComp {
  @Input()
  private id: string;
  private companyName: string;
  private units: number;
  private price: number;
  private closePrice: number;

  constructor(
    private modalService: NgbModal
    ) {}

  public params: any;

  ngOnInit() {}

  agInit(params: any): void {
    this.params = params;
    this.id = this.params.data.id;
    this.companyName = this.params.data.companyName;
    this.units = this.params.data.units;
    this.price = this.params.data.price;
    this.closePrice = this.params.data.currentprice;
  }

  refresh(): boolean {
    return false;
  }

  private closePosition(content) {
    this.modalService.open(content);
  }
}
