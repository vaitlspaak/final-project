import { ClientClosedPositionsComponent } from './client-closed-positions/client-closed-positions.component';
import { ClosePositionButtonComponent } from './close-position-button/close-position-button.component';
import { TradeModalComponent } from './trade-modal/trade-modal.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ChartsComponent } from './charts/charts.component';
import { ManagerClientsListComponent } from './manager-clients-list/manager-clients-list.component';
import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { ListStocksComponent } from './list-stocks/list-stocks.component';
import { ListStocksByIndustryComponent } from './list-stocks-by-industry/list-stocks-by-industry.component';
import { AgGridModule } from 'ag-grid-angular';
import { ManagerRoutingModule } from './manager-routing.module';
import { ListIndustryStocksComponent } from './list-industry-stocks/list-industry-stocks.component';
import { CompanyProfileComponent } from './company-profile/company-profile.component';
import { ViewCompanyButtonComponent } from './view-company-button/view-company-button.component';
import { ManagerHomeComponent } from './home/manager-home.component';
import { WatchlistEditComponent } from './watchlist-edit/watchlist-edit.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ManagerTradeButtonComponent } from './manager-trade-button/manager-trade-button';
import { ClientOpenPositionsComponent } from './client-open-positions/client-open-positions.component';
import { ClientProfileComponent } from './client-profile/client-profile.component';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { ShowUserWatchlistComponent } from './show-user-watchlist/show-user-watchlist.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ManagerOpenPositionsComponent } from './manager-open-positions/manager-open-positions.component';

@NgModule({
  declarations: [
    ManagerClientsListComponent,
    ManagerTradeButtonComponent,
    ListStocksComponent,
    ListStocksByIndustryComponent,
    ListIndustryStocksComponent,
    CompanyProfileComponent,
    ChartsComponent,
    ViewCompanyButtonComponent,
    ManagerHomeComponent,
    WatchlistEditComponent,
    TradeModalComponent,
    ClientOpenPositionsComponent,
    ClosePositionButtonComponent,
    ClientClosedPositionsComponent,
    ClientProfileComponent,
    ShowUserWatchlistComponent,
    DashboardComponent,
    ManagerOpenPositionsComponent
  ],
  imports: [
    SharedModule,
    FormsModule,
    AgGridModule.withComponents([
      ListStocksByIndustryComponent,
      ListStocksComponent,
      ClosePositionButtonComponent
    ]),
    ManagerRoutingModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    ScrollToModule.forRoot()
  ],
  entryComponents: [ViewCompanyButtonComponent]
})
export class ManagerModule {}
