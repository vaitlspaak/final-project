import {
  Component,
  OnInit,
  Input,
  Renderer2,
  ElementRef,
  ViewChild
} from '@angular/core';
import * as CanvasJS from './../../shared/canvasjs.min';
import { PriceModel } from './../../shared/models/price.model';
import { PriceService } from './../../core/price.service';

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements OnInit {
  @Input()
  private title: string;
  @Input()
  private symbol: string;
  private dataPoints: PriceModel[];
  private chartData: any[] = [];
  private width: string;
  private height: string;

  @ViewChild('chartContainer') chartContainer: ElementRef;

  constructor(
    private readonly priceService: PriceService,
    private renderer: Renderer2,
    private el: ElementRef
  ) {}

  ngOnInit() {
    this.priceService.getCompanyPrices(this.symbol).subscribe(
      (prices) => {
        this.dataPoints = prices;

        this.dataPoints.forEach((data) => {
          this.chartData.push({
            x: new Date(data.opendate),
            y: [data.lowprice, data.highprice, data.endprice, data.startprice]
          });
        });

        const chart = new CanvasJS.Chart('chartContainer', {
          animationEnabled: true,
          animationDuration: 2000,
          theme: 'dark2',
          title: {
            text: this.title
          },
          subtitles: [
            {
              text: 'Daily price movement'
            }
          ],
          axisX: {
            interval: 1,
            valueFormatString: 'D.M.Y.',
            intervalType: 'day',
            labelAngle: -45
          },
          axisY: {
            includeZero: false,
            prefix: '$',
            title: 'Price'
          },
          toolTip: {
            content:
              'Date: {x}<br /><strong>Price:</strong><br />Open: {y[0]}, Close: {y[3]}<br />High: {y[1]}, Low: {y[2]}'
          },
          data: [
            {
              type: 'candlestick',
              risingColor: '#019e4c',
              color: '#da2a4d',
              yValueFormatString: '$##0.00',
              dataPoints: this.chartData
            }
          ],
          backgroundColor: '#fafafa4f'
        });
        this.width = '100%';
        this.height = '370px';
        chart.render();
      },
      () => {}
    );
  }
}
