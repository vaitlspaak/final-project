export class ManagersNameMailDTO {
  firstname: string;
  lastname: string;
  email: string;
}
