import { NgModule } from '@angular/core';
import { AuthGuardService } from '../core/auth-guard.service';
import { AdminHomeComponent } from './home/admin-home.component';
import { ManagerAndAdminRegisterComponent } from './register/manager-admin/manager-admin-register.component';
import { ClientsRegisterComponent } from './register/client/client-register.component';
import { ManagersListComponent } from './lists/managers-list/managers-list.component';
import { ClientsListsComponent } from './lists/clients-lists/clients-lists.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    component: AdminHomeComponent,
    canActivate: [AuthGuardService],
    pathMatch: 'full'
  },
  {
    path: 'managers',
    component: ManagersListComponent,
    canActivate: [AuthGuardService],
    pathMatch: 'full'
  },
  {
    path: 'clients',
    component: ClientsListsComponent,
    canActivate: [AuthGuardService],
    pathMatch: 'full'
  },
  {
    path: 'register/manager-admin',
    component: ManagerAndAdminRegisterComponent,
    canActivate: [AuthGuardService],
    pathMatch: 'full'
  },
  {
    path: 'register/manager-admin/:id',
    component: ManagerAndAdminRegisterComponent,
    canActivate: [AuthGuardService],
    pathMatch: 'full'
  },
  {
    path: 'register/client',
    component: ClientsRegisterComponent,
    canActivate: [AuthGuardService],
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {}
