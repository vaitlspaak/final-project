import { GetManagerModel } from './../../../shared/models/getManager.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ManagersListService } from './../../../core/managers-list.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-managers-list',
  templateUrl: './managers-list.component.html',
  styleUrls: ['./managers-list.component.css']
})
export class ManagersListComponent implements OnInit {
  private managers: GetManagerModel[];
  private columns: string[];
  private userId: string;
  private searchTerm: string;

  constructor(
    private managersListService: ManagersListService,
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.getManagers();
    this.columns = this.managersListService.getColumns();
  }

  editUser(userId: string, content) {
    this.userId = userId;
    this.modalService.open(content);
  }

  getManagers() {
    this.managersListService.getManagers().subscribe((users) => {
      this.managers = users;
    });
  }
}
