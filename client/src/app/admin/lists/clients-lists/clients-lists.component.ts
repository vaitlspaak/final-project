import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Client } from './../../../../../../server/src/data/entities/client.entity';
import { ClientsListService } from './../../../core/clients-list.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-clients-lists',
  templateUrl: './clients-lists.component.html',
  styleUrls: ['./clients-lists.component.css']
})
export class ClientsListsComponent implements OnInit {
  private clients: Client[];
  private columns: string[];
  private clientId: string;
  private searchTerm: string;

  constructor(
    private clientsListService: ClientsListService,
    private modalService: NgbModal
  ) {}

  ngOnInit() {
    this.clientsListService.getClients().subscribe((client) => {
      this.clients = client;
    });

    this.columns = this.clientsListService.getColumns();
  }

  editUser(clientId: string, content) {
    this.clientId = clientId;
    this.modalService.open(content);
  }
}
