import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/auth.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-sidebar-admin',
  templateUrl: './sidebar-admin.component.html',
  styleUrls: ['../../../assets/css/sidebar-menu.css']
})
export class AdminSidebarComponent implements OnInit {

  constructor(
    private router: Router,
    private authService: AuthService,
    private readonly notificator: ToastrService) { }

  ngOnInit() { }

  public adminRoute(): void {
    this.router.navigateByUrl('admin/home');
  }

  public adminManagersRoute(): void {
    this.router.navigateByUrl('admin/managers');
  }

  public adminClientsRoute(): void {
    this.router.navigateByUrl('admin/clients');
  }

  public registerManagersAdminsRoute(): void {
    this.router.navigateByUrl('admin/register/manager-admin');
  }

  public registerClientsRoute(): void {
    this.router.navigateByUrl('admin/register/client');
  }

  public logout() {
    this.authService.logoutUser();
    this.router.navigate(['login']);
    this.notificator.success(
      `You've been successfully logged out of the system `,
      'Logged out'
    );
  }
}
