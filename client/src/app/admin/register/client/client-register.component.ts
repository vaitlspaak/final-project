import { ClientAddEditModel } from './../../../shared/models/clientAddEdit.model';
import { ActivatedRoute } from '@angular/router';
import { ManagersListService } from './../../../core/managers-list.service';
import { ManagersNameMailDTO } from './../../models/managersNameMail.dto';
import { ClientsService } from './../../../core/client.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-client-register',
  templateUrl: './client-register.component.html',
  styleUrls: ['./client-register.component.css']
})
export class ClientsRegisterComponent implements OnInit {
  private clientDataForm: FormGroup;
  private managersList: ManagersNameMailDTO[];
  private clientFound: ClientAddEditModel;
  private currentManager: string;
  @Input()
  private clientId: string;
  private visibleAddClient: string;
  private visibleEditClient: string;

  constructor(
    private fb: FormBuilder,
    private clientService: ClientsService,
    private managersListService: ManagersListService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.clientDataForm = this.fb.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', Validators.required],
      address: ['', Validators.required],
      photoUrl: [''],
      age: ['', Validators.required],
      managerId: [
        'Please select a manager for this client',
        Validators.required
      ],
      funds: ['', Validators.required]
    });

    this.activatedRoute.params.subscribe((params) => {
      if (this.clientId) {
        this.visibleEditClient = 'hidden';
        this.clientService.getClientById(this.clientId).subscribe((client) => {
          this.clientFound = client;

          this.currentManager = `${this.clientFound.manager.firstname} ${
            this.clientFound.manager.lastname
          } - ${this.clientFound.manager.email}`;

          this.clientDataForm.patchValue({
            firstname: `${this.clientFound.firstname}`,
            lastname: `${this.clientFound.lastname}`,
            email: `${this.clientFound.email}`,
            address: `${this.clientFound.address}`,
            photoUrl: '',
            age: `${this.clientFound.age}`,
            managerId: 'Keep current manager',
            funds: `${this.clientFound.funds.currentamount}`
          });
        });
      } else {
        this.visibleAddClient = 'hidden';
      }
    });

    this.managersListService.getManagers().subscribe((user) => {
      this.managersList = user;
    });
  }

  addClient(): void {

    this.clientService.addClient(this.clientDataForm.value);
  }

  editClientData(clientId: string): void {

    this.clientService.editClientData(this.clientDataForm.value, clientId);
  }
}
