import { ManagerAdminService } from './../../../core/manager-admin.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Input } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'my-auth-token'
  })
};
@Component({
  selector: 'app-manager-admin-register',
  templateUrl: './manager-admin-register.component.html',
  styleUrls: ['./manager-admin-register.component.css']
})
export class ManagerAndAdminRegisterComponent implements OnInit {
  private managerRegister: FormGroup;
  @Input()
  private userId: string;
  constructor(
    private http: HttpClient,
    private fb: FormBuilder,
    private readonly notificator: ToastrService,
    private readonly router: Router,
    private managerOrAdminService: ManagerAdminService
  ) {}

  ngOnInit() {
    this.managerRegister = this.fb.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
      role: ['', Validators.required]
    });
  }

  addManagerOrAdmin() {
    const data = JSON.stringify(this.managerRegister.value);
    this.managerOrAdminService.addManagerOrAdmin(data);
  }
}
