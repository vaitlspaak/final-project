import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { AdminHomeComponent } from './home/admin-home.component';
import { ClientsListsComponent } from './lists/clients-lists/clients-lists.component';
import { ManagersListComponent } from './lists/managers-list/managers-list.component';
import { ClientsRegisterComponent } from './register/client/client-register.component';
import { ManagerAndAdminRegisterComponent } from './register/manager-admin/manager-admin-register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminSidebarComponent } from './sidebar-admin/sidebar-admin.component';

@NgModule({
  declarations: [
    AdminHomeComponent,
    ClientsListsComponent,
    ManagersListComponent,
    ClientsRegisterComponent,
    ManagerAndAdminRegisterComponent,
    AdminSidebarComponent
  ],

  imports: [
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    AdminRoutingModule,
    NgbModule.forRoot()
  ]
})
export class AdminModule {}
