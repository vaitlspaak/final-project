import { PipeTransform, Pipe } from '@angular/core';
import { Client } from '../../../../server/src/data/entities/client.entity';

@Pipe({
    name: 'clientsFilter'
})
export class ClientsFilterPipe implements PipeTransform {
    transform(clients: Client[], searchTerm: string): Client[] {

        if (!clients || !searchTerm) {
            return clients;
        }

        // tslint:disable-next-line:no-shadowed-variable
        return clients.filter(clients =>
            clients.firstname.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1);
    }
}
