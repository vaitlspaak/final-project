import { PriceModel } from './../shared/models/price.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PriceService {
  constructor(private http: HttpClient) {}

  getCompanyPrices(symbol: string): Observable<PriceModel[]> {
    return this.http
      .get(`http://localhost:3000/prices/company/${symbol}`)
      .pipe(map((prices) => <PriceModel[]>prices));
  }
}
