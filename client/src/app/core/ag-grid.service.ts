import { Injectable } from '@angular/core';

@Injectable()

export class AgGridService {

    constructor() { }

    public currencyUSDRenderer(params: any) {

        const usdFormate = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 0
        });
        return usdFormate.format(params.value);
    }

    public currencyPercentRenderer(params: any) {
        return params.value + '%';
    }
}
