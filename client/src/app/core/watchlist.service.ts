import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { AddWatchlistModel } from './../shared/models/addWatchlist.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WatchlistService {
  constructor(
    private readonly http: HttpClient,
    private readonly notificator: ToastrService,
    private readonly router: Router
  ) {}

  addWatchList(watchlistData: AddWatchlistModel) {

    this.http
      .post(
        'http://localhost:3000/watchlist/add',
        JSON.stringify(watchlistData)
      )
      .subscribe(
        () => {
          this.notificator.success('Watchlist added to DB');
        },
        (httpErrorResponse) => {
          console.log(httpErrorResponse);

          this.notificator.error(
            'Watchlist with this name already exists or application cannot connect to DB'
          );
        }
      );
  }
}
