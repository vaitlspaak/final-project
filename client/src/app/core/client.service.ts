import { ClientsListsComponent } from './../admin/lists/clients-lists/clients-lists.component';
import { GetClientFinancialDataModel } from './../shared/models/getClientFinancialData.model';
import { GetClosedPositionModel } from './../shared/models/getClosedPosition.model';
import { OpenPositionModel } from './../shared/models/openPosition.model';
import { Observable } from 'rxjs';
import { ClientAddEditModel } from './../shared/models/clientAddEdit.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { map } from 'rxjs/operators';
import { ModalService } from './modal.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};
@Injectable()
export class ClientsService {
  constructor(
    private notificator: ToastrService,
    private router: Router,
    private http: HttpClient,
    private modalService: ModalService
  ) {}

  public addClient(clientRegister: JSON) {

    this.http
      .post(
        'http://localhost:3000/admin/register/client',
        clientRegister,
        httpOptions
      )
      .subscribe(
        () => {
          this.notificator.success('New client added to DB');
          this.modalService.closeModal();
        },
        (httpErrorResponse) => {
          console.log(httpErrorResponse);

          this.notificator.error(
            'Client with this data already exist in the DB or application cannot connect to DB'
          );
          this.router.navigate(['ClientsListsComponent']);
        }
      );
  }

  public editClientData(clientRegister: ClientAddEditModel, clientId: string) {
    this.http
      .put(
        `http://localhost:3000/admin/register/client/${clientId}`,
        clientRegister,
        httpOptions
      )
      .subscribe(
        () => {
          this.notificator.success('Client updated in DB');
          this.modalService.closeModal();
        },
        (httpErrorResponse) => {
          console.log(httpErrorResponse);

          this.notificator.error(
            'Error saving data in the DB or application cannot connect to DB'
          );
          this.router.navigate(['/server-error']);
        }
      );
  }
  public getClientById(id: string): Observable<ClientAddEditModel> {
    return this.http
      .get(`http://localhost:3000/admin/register/client/${id}`)
      .pipe(map((client) => <ClientAddEditModel>client));
  }

  public getClientAvailableMoney(id: string): Observable<number> {
    return this.http
      .get(`http://localhost:3000/admin/register/client/amount/${id}`)
      .pipe(map((amount) => <number>amount));
  }

  public getClientOpenPositions(id: string): Observable<OpenPositionModel[]> {
    return this.http
      .get(`http://localhost:3000/order/open/${id}`)
      .pipe(map((positions) => <OpenPositionModel[]>positions));
  }

  public getClientFinancialInfo(
    clientId: string
  ): Observable<GetClientFinancialDataModel> {
    return this.http
      .get(`http://localhost:3000/clients/fin/${clientId}`)
      .pipe(map((financialData) => <GetClientFinancialDataModel>financialData));
  }

  public getClientClosedPositions(
    id: string
  ): Observable<GetClosedPositionModel[]> {
    return this.http
      .get(`http://localhost:3000/order/closed/${id}`)
      .pipe(map((positions) => <GetClosedPositionModel[]>positions));
  }
}
