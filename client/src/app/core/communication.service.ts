import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class CommunicationService {
  public isManagingSubject$ = new BehaviorSubject<boolean>(false);
  public isExitButtonvsible$ = new BehaviorSubject<boolean>(false);

  constructor() {}
}
