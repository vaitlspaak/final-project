import { ModalService } from './modal.service';
import { ClientAddEditModel } from './../shared/models/clientAddEdit.model';
import { ManagerOrAdminAddEditModel } from './../shared/models/managerOrAdmin.model';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ManagerAdminService {
  constructor(
    private readonly notificator: ToastrService,
    private readonly router: Router,
    private http: HttpClient,
    private modalService: ModalService
  ) {}
  addManagerOrAdmin(data: string) {
    this.http
      .post(
        'http://localhost:3000/admin/register/manager-admin',
        data,
        httpOptions
      )
      .subscribe(
        () => {
          this.notificator.success('New manager or admin added to DB');
          this.modalService.closeModal();
        },
        (httpErrorResponse) => {
          this.notificator.error(
            'Мanager or admin with this data already exist in the DB or application cannot connect to DB'
          );
          this.router.navigate(['/server-error']);
        }
      );
  }

  getManagerOrAdminById(id: string): Observable<ManagerOrAdminAddEditModel> {
    return this.http
      .get(`http://localhost:3000/admin/register/manager-admin/${id}`)
      .pipe(map((manager) => <ManagerOrAdminAddEditModel>manager));
  }

  getClientByManagerId(id: string): Observable<ClientAddEditModel[]> {
    return this.http
      .get(`http://localhost:3000/admin/manager-admin/clients/${id}`)
      .pipe(map((clients) => <ClientAddEditModel[]>clients));
  }
}
