import { ClientOpenPositionsComponent } from './../manager/client-open-positions/client-open-positions.component';
import { SectorService } from './sector.service';
import { StocksService } from './stocks.service';
import { AuthGuardService } from './auth-guard.service';
import { ClientsService } from './client.service';
import { ClientsListService } from './clients-list.service';
import { SidebarService } from './sidebar.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { RequesterService } from './requester.service';
import { StorageService } from './storage.service';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from './auth.service';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { ManagersListService } from './managers-list.service';
import { SpinnerService } from './spinner.service';
import { MarketService } from './market.service';
import { AgGridService } from './ag-grid.service';
import { CommunicationService } from './communication.service';

@NgModule({
  providers: [
    RequesterService,
    AuthService,
    ToastrService,
    StorageService,
    JwtHelperService,
    ManagersListService,
    ClientsService,
    SidebarService,
    ClientsListService,
    StocksService,
    SectorService,
    AuthGuardService,
    SpinnerService,
    MarketService,
    AgGridService,
    CommunicationService,
    ClientOpenPositionsComponent
  ]
})
export class CoreModule {
  public constructor(@Optional() @SkipSelf() parent: CoreModule) {
    if (parent) {
      throw new Error('Core module is already provided!');
    }
  }
}
