import { Client } from './../../../../server/src/data/entities/client.entity';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable()

export class ClientsListService {

    constructor(private http: HttpClient) { }

    getClients(): Observable<Client[]> {
        return this.http
            .get('http://localhost:3000/admin/clients')
            .pipe(map((x) => <Client[]>x));
    }

    getColumns(): string [] {
        return ['First Name', 'Last Name', 'Age', 'Email', 'Date Registered', 'Funds'];
    }

    getColumnsForTrade(): string [] {
        return ['First Name', 'Last Name', 'Age', 'Email', 'Date Registered', 'Funds', 'Trade For This Client'];
    }

}
