import { SectorModel } from './../shared/models/sector.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable()
export class SectorService {
  constructor(private http: HttpClient) {}

  getAllSectors(): Observable<SectorModel[]> {
    return this.http
      .get('http://localhost:3000/sector')
      .pipe(map((sector) => <SectorModel[]>sector));
  }
}
