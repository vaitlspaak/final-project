import { ClientOpenPositionsComponent } from './../manager/client-open-positions/client-open-positions.component';
import { Router } from '@angular/router';
import { ClosePositionModel } from './../shared/models/closePosition.model';
import { ToastrService } from 'ngx-toastr';
import { ModalService } from './modal.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ClientsService } from './client.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PositionDataModel } from './../shared/models/positionData.model';
import { Injectable } from '@angular/core';
import { GetClientIdModel } from './../shared/models/getClientId.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class MarketService {
  constructor(
    private readonly http: HttpClient,
    private clientService: ClientsService,
    private modalService: ModalService,
    private notificator: ToastrService,
    private router: Router,
    private clientOpenPositionComponent: ClientOpenPositionsComponent
  ) {}

  public openTrade(positionData: PositionDataModel) {
    this.http
      .post('http://localhost:3000/order', positionData, httpOptions)
      .subscribe(
        () => {
          this.notificator.success(
            `New ${positionData.type} position opened at ${positionData.price}`
          );

          this.modalService.closeModal();
        },
        (error) => {
          console.log(error);
          this.notificator.error(`Cannot connect to server.`);
        }
      );
  }

  public closeTrade(closePositionData: ClosePositionModel) {

    this.http
      .put('http://localhost:3000/order/close/', closePositionData)
      .subscribe(
        () => {
          this.notificator.success(`Position closed`);
          this.modalService.closeModal();
        },
        (error) => {
          console.log(error);

          this.notificator.error(`Error closing the position`);
        }
      );
  }
  public getUsersAvailableFunds(id: string): Observable<number> {
    return this.clientService.getClientAvailableMoney(id);
  }

  public getClientsWithOpenedPositions(): Observable<GetClientIdModel[]> {
    return this.http
      .get('http://localhost:3000/order/clients')
      .pipe(map((clientsId) => <GetClientIdModel[]>clientsId));
  }
}
