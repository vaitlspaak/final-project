import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpHeaders } from '@angular/common/http';
import { UserLoginModel } from './../admin/models/userLogin.model';
import { StorageService } from './storage.service';
import { Injectable } from '@angular/core';
import { RequesterService } from './requester.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    Authorization: 'my-auth-token'
  })
};
@Injectable()
export class AuthService {

  private readonly isLoggedInSubject$ = new BehaviorSubject<boolean>(
    this.hasToken()
  );

  public constructor(
    private readonly storageService: StorageService,
    private readonly requester: RequesterService,
    public jwtHelper: JwtHelperService
  ) { }


  public get isLoggedIn$(): Observable<boolean> {
    return this.isLoggedInSubject$.asObservable();
  }

  public registerUser(user: UserLoginModel): Observable<any> {
    return this.requester.post(
      'http://localhost:3000/auth/register',
      JSON.stringify(user)
    );
  }

  public loginUser(user: UserLoginModel): Observable<any> {
    return this.requester
      .post(
        'http://localhost:3000/auth/login',
        JSON.stringify(user),
        httpOptions.headers
      )
      .pipe(
        tap((response) => {
          this.storageService.setItem('token', (<any>response).token);
          this.isLoggedInSubject$.next(true);
        })
      );
  }

  public logoutUser(): void {
    this.storageService.removeItem('token');
  }

  private hasToken(): boolean {
    return !!this.storageService.getItem('token');
  }

  // Check whether the token is expired and return true or false
  public isAuthenticated(): boolean {
    const token = localStorage.getItem('token');
    return !this.jwtHelper.isTokenExpired(token);
  }
}
