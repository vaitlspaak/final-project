import { CompanyModel } from './../shared/models/company.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable()
export class StocksService {
  constructor(private http: HttpClient) {}

  getAllCompaniesData(): Observable<CompanyModel[]> {
    return this.http.get('').pipe(map((company) => <CompanyModel[]>company));
  }

  getAllCompaniesBySector(industryId: string): Observable<CompanyModel[]> {
    return this.http
      .get(`http://localhost:3000/company/industry/${industryId}`)
      .pipe(map((company) => <CompanyModel[]>company));
  }

  getCompanyDataBySymbol(symbol: string): Observable<CompanyModel> {
    return this.http
      .get(`http://localhost:3000/company/${symbol}`)
      .pipe(map((company) => <CompanyModel>company));
  }
}
