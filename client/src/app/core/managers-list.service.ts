import { GetManagerModel } from './../shared/models/getManager.model';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable()
export class ManagersListService {
  constructor(private http: HttpClient) {}
  getManagers(): Observable<GetManagerModel[]> {
    return this.http
      .get('http://localhost:3000/admin/managers')
      .pipe(map((manager) => <GetManagerModel[]>manager));
  }

  getColumns(): string[] {
    return ['First Name', 'Last Name', 'Email', 'Date Registered', 'User type'];
  }
}
