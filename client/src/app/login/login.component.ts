import { AuthService } from './../core/auth.service';
import { StorageService } from './../core/storage.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private loginForm: FormGroup;
  constructor(
    private readonly fb: FormBuilder,
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly notificator: ToastrService,
    private readonly storageservice: StorageService,
    private readonly jwtHelperService: JwtHelperService
  ) {}

  ngOnInit() {

    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });
  }

  public login(): void {
    this.authService.loginUser(this.loginForm.value).subscribe(
      () => {
        this.notificator.success('Logged in successfully!');
        if (this.jwtHelperService.decodeToken().role === 'manager') {
          this.router.navigate(['/manager/home']);
        } else if (this.jwtHelperService.decodeToken().role === 'admin') {
          this.router.navigate(['/admin/clients']);
        }
      },
      (httpErrorResponse) => {
        this.notificator.error(
          'Login failed!',
          `${httpErrorResponse.error.message}`
        );
      }
    );
  }
}
