![Main42](https://database.bluecherrystudio.com/Main42/Main42-README.md-Logo.jpg)

## Investment management system tool with charts/diagrams and functionalities helping you manage your clients' stocks portfolios.

# Functionalities and Project Description:

- **Public part** (accessible without authentication)
- **Private part** (available for portfolio managers and admins). Based on what each user can do the application has two main sections:
  - **Portfolio management** section.
  - **Admin** section.

### Public Part

- Because of project's specifics the only **public part** of the current project is the login form of the application.

### Private Part(Portfolio management section)

The portfolio management section could be accessed only by **Portfolio managers**. This section has two modes - **manager mode and client mode**. Both modes are operated **ONLY** by a portfolio manager

- **When in manager mode** a portfolio manager **CAN**:

  - See a **list of all open position** of their clients.
  - See a **list of all assigned clients**.
  - See **the current price and price history of more than 5000 stocks** separated in 12 industries without being able to trade
  - Enter in **client mode** by selecting a user

- **When in client mode** a portfolio manager **CAN**:

  - **Trade** more than 5000 stocks on behalf of a client

    - **Open** Buy or Sell position for a client
    - **Close** opened positions

  - See **trading history** of each of thier clients
  - **Exit client mode** whenever they want

### Private Part(Admin section)

The admin section could be accessed only by **Admins**

- An admin **CAN**:

  - **Add** and **edit** clients data.
  - **Assign** and **Reassign** clients to managers
  - **Add** other admins or managers to the system
  - **Search** for clients, admins or managers registered in the the system

## **Database structure**

![Main42 DB structure](https://database.bluecherrystudio.com/Main42/Schema.jpg)

## **Installation**

### In order to be able to start the application you need to have mySQL database installed and be sure to run **_npm install_** both in your **client** and **server** folders

### 1. Create mySQL schema named **main42**

### 2. Open console, navigate to your "server" folder and:

    1. Run npm install
    2. Run the following command "npm run migration:generate -- -n Init"
    3. Then run "npm run migration:run"
    4. Go to ormconfig.js file and set the connection to your DB server. Default values are:

    - Hostname: **localhost**
    - Username: **root**
    - Password: **root**

    5. Run "npm run setup". This command will initialize basic data in the DB: create two manager profiles, one admin profile and initialize for you the database of 5000+ stocks available on the platform

        ## Profiles created with the above command
        Manager user1:
        - username: peko@main42.com
        - password: peko1234

        Manager user2:
        - username: dani@main42.com
        - password: dani1234

        Admin user:
        - username: admin@main42.com
        - password: admin1234


    6. Again in the console in **server** folder write : "npm run update" . This command will generate fake price data for all stocks in the DB (This command may take a little time)

    7. Once all initial data is added in **server** folder of your console write "npm run start". This command will start your backend server.

### 3. Navigate to your "client" folder and execute "ng serve -o" command. This command will open your default browser and start the application for you.

### 4. Use the above usernames and passwords to login in both application sections - **Portfolio management** && **Admin**

### **Team Members**

- Danail Kirilov (@my.telerikacademy.com - vaitlspaak@gmail.com)
- Petar Todorov (@my.telerikacademy.com - petar.ptodorov@gmail.com)

### Gitlab repository:

- Link: https://gitlab.com/vaitlspaak/final-project

### Thanks to Telerik Academy Team

    Specal thanks to Martin Veshev and Rosen Urkov
